
import sys, os, glob, re, subprocess, nltk
import pandas as pd

from docx import Document
from pathlib import Path

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import decomposition, ensemble
from sklearn.externals import joblib

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix 

def readDoc(dir):
    path = []
    files = os.listdir()
    pathlist = Path(dir).glob('**/*.docx')
    for p in pathlist:
        path.append(p)

    print('Number of files: ', len(path))

    data = loadData()

    count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}')
    count_vect.fit(data.text)

    model = joblib.load('model/MNBcount.jolib')

    current_directory = os.getcwd()
    final_directory = os.path.join(current_directory, r'classified')
    if not os.path.exists(final_directory):
        os.makedirs(final_directory)

    current_directory = current_directory + '/classified/classified_'

    for i in path:
        f = open(i, 'rb')
        doc = Document(f)
        f.close()

        tt = []
        for p in doc.paragraphs:
            tok = nltk.tokenize.sent_tokenize(p.text.strip())
            for t in tok:
                tt.append(t)

        doc = Document()

        for s in tt:
            c = model.predict(count_vect.transform([s]))
            if c == 1:
                doc.add_paragraph(s)

        name = str(i)
        name = re.sub(r'(.*)/', '', name)
        doc.save(current_directory + name.strip())

    print('step extraction processing => done')
    subprocess.call(['java', '-jar', 'model/recipeExt.jar', 'classified'])
    print('recipe extraction processing => done')


def loadData():

    #data loading
    rel = open('model/rel', 'r+', encoding='latin-1').read()
    nrel = open('model/non-rel', 'r+', encoding='latin-1').read()

    label, text = [], []
    for i, line in enumerate(rel.split('\n')):
        label.append(1)
        text.append(line)

    for i, line in enumerate(nrel.split('\n')):
        label.append(0)
        text.append(line)

    df = pd.DataFrame()
    df['lablel'] = label
    df['text'] = text

    return df

if __name__ == '__main__':
	readDoc(sys.argv[1])

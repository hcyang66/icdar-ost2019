import nltk


"""Constants"""
"""
Common shorthand:
pos - parts of speech
np - noun phrase
"""

"""Configuration keys"""
EXTR_LOC_KEY = 'extractor_path'
GAZ_LOC_KEY = 'gazetteer_path'
INP_ITEMS_KEY = 'items_to_process'
INP_KEY = 'input_path'
RECURSE_KEY = 'recursive'
OUT_KEY = 'output_path'
#OUT DB UPLOAD
OUT_DB_UPLOAD = 'upload_to_database'
OUT_PDF_KEY = 'output_original_pdf'
EXTR_IMG_KEY = 'extract_images'
EXTR_TXT_KEY = 'extract_text'
ANALYZE_KEY = 'analyze_text'
CLSSFY_KEY = 'classify_text'
TRAIN_CLSSFR_KEY = 'train_classifier'
FILE_IDS_KEY = 'file_ids'

# TODO: make sure database keys are synced up with these constants
# Data type keys
RAW_KEY = 'raw_text'
ML_WORDS_FIX_KEY = 'multiline_word_fix_text'

TEXT_KEY = 'text_plain'
SENTS_EXTR_KEY = 'sentences'
SENTENCE_KEY = 'sentence'
SECTIONS_KEY = 'extracted_sections'
ABSTRACT_KEY = 'abstract'
INTRO_KEY = 'introduction'
RESULTS_KEY = 'results'
# possible that results and discussion may be split
DISCUSS_KEY = 'discussion'

EXPERIMENT_KEY = 'experimentation'
CONCL_KEY = 'conclusion'
ACKNOWLEDGE_KEY = 'acknowledgements'
REFERENCE_KEY = 'references'
UNTAGGED_SENTS_KEY = 'untagged_sentence_tokens'
UNTAGGED_WORDS_KEY = 'untagged_word_tokens'
TAGGED_WORDS_KEY = 'tagged_word_tokens'
TAGGED_SENTS_KEY = 'tagged_sentence_tokens'
GAZETTEER_MATCHES_KEY = 'gazetteer_search_results'
NP_CHUNKS_KEY = 'noun_phrase_chunks'
AUTHORS_KEY = 'authors'

# noun phrase chunk patterns (uses regex)
# more info here: http://www.nltk.org/book/ch07.html?#sec-chunking
NOUN_PHRASE_PATTERNS = r"""
    NP: {<([DA]T|CD)>?<N.*>+}
        {<(J|VBN).*>+<N.*>+}
        {<([DA]T|[JN]).*>+<VB[GZ]>+}
        {<RB.*>+<(J|VBN).*>+<(N.*|VB[GZ])>+}
        {<J.*>+<(CC|J.*)>*<(N.*|VB[GZ])>+}
"""
NP_KEY = 'NP'  # must match label in pattern

# TODO: improve the chunking pattern for titles
TITLE_PATTERNS = r"""
    title:  {<DT>?<NNP.*><TO|IN|NNP.*>+<NNP.*>+}
            }<.>{
"""
TITLE_KEY = 'title'  # must match label in pattern


ENGL_WORDS = set(nltk.corpus.words.words())

#Minimum threshold for image height and width
MIN_WIDTH = 40
MIN_HEIGHT = 40

import helpers
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
import nltk
import string
from sklearn import metrics

print("Loading paths....")
document_paths = helpers.file_list('/home/sneha/Developer/recipes/testingdocs', recursive=True, file_types=['.txt'])


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems


def clean(documents):
    print("Cleaning....")
    result = {}
    i = 0
    for path in documents:
        try:
            with open(path) as d:
                result[i] = d.read().lower().translate(string.punctuation)
                i += 1
        except:
            print("Cannot decode: {}".format(path))
    return result


stemmer = PorterStemmer()

"""
Define the vectorizer.
This will be how we generate the features for the classifier.
We could change the tokenize function to remove stop words using NLTK
if the scikit stopwords are not sufficient.
The vocabulary parameter are the words we are looking for.
Currently, these are used as features.
Other parameters to this function can be adjusted as needed.
"""
tfidf = TfidfVectorizer(
    tokenizer=tokenize,
    min_df=1,
    stop_words='english',
    vocabulary=helpers.read_gazetteer_to_list('../gazetteer.txt')
)

print("____tfidf matrix_____")

# this is some sample code for
# if we just want to generate the TF-IDF for all documents without classifying
result = tfidf.fit_transform(
    clean(document_paths).values()
)

print(result)

feature_names = tfidf.get_feature_names()
relevant = 0
result_transformed = result.toarray().tolist()
with open("relevant.txt", 'w') as out:
    for i, k in enumerate(result_transformed):
        if sum(k) > 0:
            relevant += 1
            out.write('{}\n'.format(document_paths[i]))

print(relevant)
print(len(result_transformed))
print(len(document_paths))

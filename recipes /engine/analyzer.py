from nltk.stem.porter import *
from helpers import is_not_gibberish
from constants import *
import os
import nltk

"""
Common shorthand:
pos - parts of speech
np - noun phrase
"""


def analyze_data(raw_data, gazetteer_path):
    """
    Given a string containing some body of text and the path to the
    gazetteer, return a dictionary containing the results of analyzing
    the text and extracting specific information.

    Currently, a fix is applied for words that are split between
    multiple lines in the text. All of the different information
    extraction and analysis is run on both the raw, unedited data and
    the fixed data. NLTK is used for word tokenization, sentence
    tokenization, parts-of-speech (pos) tagging, and chunking.

    :param raw_data: The contents of a document
    :type raw_data: str
    :param gazetteer_path: An absolute or relative path to a gazetteer
    :type gazetteer_path: str
    :return: A dictionary containing the results of analysis and
        information extraction
    :rtype: dict(...)
    """
    gazetteer_path = os.path.abspath(gazetteer_path)
    print('analyzing...')

    """
    There are several versions of the data.
    There is the raw data, which is just the text exactly as it was entered.
    There is the ml data, which is short for multi-line word fix, which just
    means that it is the result of taking the raw data and applying the
    multi-line word fix function to it.
    For each of these, there may or may not be two versions: one version will
    still have all of the original line breaks, and one will not have any line
    breaks at all.

    raw - exactly as entered
    ml - raw data after multi-line word fix has been applied
    """
    analysis_results = {
        RAW_KEY: all_analysis_results(
            data_to_analyze=raw_data,
            gazetteer_path=gazetteer_path
        )
    }

    ml_fix_data = fix_multiline_words(
        raw_str=raw_data,
        valid_words=ENGL_WORDS,
        stemmer=PorterStemmer()
    )

    if ml_fix_data is not None:
        analysis_results[ML_WORDS_FIX_KEY] = all_analysis_results(
            data_to_analyze=ml_fix_data,
            gazetteer_path=gazetteer_path
        )

    return analysis_results


def all_analysis_results(data_to_analyze, gazetteer_path):
    """
    Given a string containing the body of some document and the path to
    a gazetteer, return the results of running multiple different kinds
    of analysis and information extraction on the text.

    :param data_to_analyze: The body of a document
    :type data_to_analyze: str
    :param gazetteer_path: An absolute or relative path to a gazetteer
    :type gazetteer_path: str
    :return: A dictionary containing the results of running all the
        different kinds of analysis and information extraction on the
        given text
    :rtype: dict(...)
    """
    gazetteer_path = os.path.abspath(gazetteer_path)

    """
    Untagged sentence tokens

    {
        UNTAGGED_SENTS_KEY: [
            'This is a complete sentence.',
            'This is another complete sentence!',
            ...
        ]
    }

    dict(str: list(str, str, ...))
    """
    data = {UNTAGGED_SENTS_KEY: nltk.tokenize.sent_tokenize(data_to_analyze)}
    """
    The 'data' dictionary is only really used internally to store the data that
    is needed to extract the information that we actually care about. The info
    that is returned is stored in the 'results' dictionary. We currently only
    return a string containing the original text that was passed in for
    analysis, the experimentally extracted abstract, a list of experimentally
    extracted authors, a list of experimentally extracted titles, and a copy of
    each sentence with the gazetteer matches found in it and the noun phrase
    chunks included.
    """

    """
    Adding untagged sentence tokens to the results

    {SENTS_EXTR_KEY: {
        index: {
            SENTENCE_KEY: 'This is a complete sentence.'
        },
        ...
    }}

    dict(str: dict(
        int: dict(
            str: str
        )
    ))
    """
    num_sentences = len(data[UNTAGGED_SENTS_KEY])
    results = {
        SENTS_EXTR_KEY: {
            i: {
                SENTENCE_KEY: data[UNTAGGED_SENTS_KEY][i]
            } for i in range(num_sentences)
        }
    }

    """
    Untagged word tokens

    {UNTAGGED_WORDS_KEY: ['Each', 'item', 'is', 'a', 'word', ...]}

    dict(str: list(str, str, ...))
    """
    data[UNTAGGED_WORDS_KEY] = list(filter(lambda w: is_not_gibberish(w), nltk.word_tokenize(data_to_analyze)))

    """
    POS-tagged word tokens

    {TAGGED_WORDS_KEY: [('Each', 'tag'), ('item', 'tag'), ...]}

    dict(str: list(tuple(str, str), tuple(str, str), ...))

    The values of the tags for different words and parts of speech will depend
    on the tagset that is used.
    """
    data[TAGGED_WORDS_KEY] = nltk.pos_tag(data[UNTAGGED_WORDS_KEY])

    """
    POS-tagged sentence tokens

    {TAGGED_SENTS_KEY: [
        [
            ('This', 'tag'),
            ('is', 'tag'),
            ('a', 'tag'),
            ('complete', 'tag'),
            ('sentence', 'tag'],
            ('.', 'tag')
        ],
        [
            ('Each', 'tag'),
            ('item', 'tag'),
            ('is', 'tag'),
            ...
        ],
        ...
    }
    dict(str: list(list(tuple(str, str))))

    Elements of inner lists are word-tag pairs that comprise complete sentences.
    """
    data[TAGGED_SENTS_KEY] = nltk.pos_tag_sents(list(map(
        lambda x: nltk.word_tokenize(x),
        data[UNTAGGED_SENTS_KEY]
    )))

    """
    Gazetteer matches

    {GAZETTEER_MATCHES_KEY: {
        index: ['keyword', 'keyword', ...],
        index: [],  # no keywords found in the sentence at that index
        ...
    }}

    dict(str: dict(
        int: list(str)
    ))
    """
    data[GAZETTEER_MATCHES_KEY] = gazetteer_matches(
        sentences=data[UNTAGGED_SENTS_KEY],
        gazetteer=gazetteer_path
    )

    # add gazetteer matches to results
    for key in results[SENTS_EXTR_KEY]:
        results[SENTS_EXTR_KEY][key].update(
            {GAZETTEER_MATCHES_KEY: data[GAZETTEER_MATCHES_KEY][key]}
        )

    """
    Noun phrase chunks

    {NP_CHUNKS_KEY: {
        index: {
             NP_KEY: ['chunk', 'chunk', ...]
        },
        ...
    }}

    dict(str: dict(
        int: dict(
            str: list(str)
        )
    ))
    """
    np_chunk_parser = nltk.RegexpParser(NOUN_PHRASE_PATTERNS)

    data[NP_CHUNKS_KEY] = chunk_word_patterns_in_sentences(
        tagged_sentences=data[TAGGED_SENTS_KEY],
        parsing_fcn=np_chunk_parser.parse,
        chunk_tags=[NP_KEY]
    )

    # add noun phrase chunks to results
    for key in results[SENTS_EXTR_KEY]:
        results[SENTS_EXTR_KEY][key].update(data[NP_CHUNKS_KEY][key])

    """
    Data sections - Title, Author, and Abstract

    {SECTIONS_KEY: {
        TITLE_KEY: [
            'A Potential Title',
            'Another Potential Title',
        ],
        AUTHORS_KEY: [
            'Possible Author',
            'Potential Author',
            ...
        ],
        ABSTRACT_KEY: 'Experimentally extracted abstract.'
    }}

    dict(str: dict(str: list(str)))
    """
    title_chunk_parser = nltk.RegexpParser(TITLE_PATTERNS)

    results[SECTIONS_KEY] = chunk_word_patterns(
        tagged_words=data[TAGGED_WORDS_KEY],
        parsing_fcn=title_chunk_parser.parse,
        chunk_tags=[TITLE_KEY]
    )

    results[SECTIONS_KEY].update(
        {AUTHORS_KEY: chunk_word_patterns(data[TAGGED_WORDS_KEY])['PERSON']}
    )

    # section extraction
    sections = section_extraction(data[UNTAGGED_WORDS_KEY])

    results[SECTIONS_KEY][ABSTRACT_KEY] = sections[0]
    results[SECTIONS_KEY][INTRO_KEY] = sections[1]
    results[SECTIONS_KEY][EXPERIMENT_KEY] = sections[2]
    results[SECTIONS_KEY][RESULTS_KEY] = sections[3]
    results[SECTIONS_KEY][CONCL_KEY] = sections[4]
    results[SECTIONS_KEY][ACKNOWLEDGE_KEY] = sections[5]
    results[SECTIONS_KEY][REFERENCE_KEY] = sections[6]

    # add abstract to results
    # results[SECTIONS_KEY][ABSTRACT_KEY] = paper_abstract(
    #    data[UNTAGGED_WORDS_KEY]

    """
    Raw data

    {TEXT_KEY: 'plain text'}

    dict(str: str)
    """
    results[TEXT_KEY] = data_to_analyze

    """
    Results

    {
        TEXT_KEY: 'Original text',
        SECTIONS_KEY: {
            TITLE_KEY: ['Potential Title #1', 'Potential Title #2',...],
            AUTHORS_KEY: ['Author #1', 'Author #2', ...],
            ABSTRACT_KEY: 'Experimentally extracted abstract.'
        },
        SENTS_EXTR_KEY: {
            index: {
                SENTENCE_KEY: 'This is a complete sentence.',
                GAZETTEER_MATCHES_KEY: ['keyword', 'keyword', ...],
                NP_KEY: ['a complete sentence']
            },
            index: {...},
            ...
        }
    }

    dict(
        str: str,
        str: dict(
            str: list(str),
            str:list(str),
            str: list(str)
        ),
        str: dict(
            int: dict(
                str: str,
                str: list(str),
                str: list(str)
            )
        )
    )
    """
    return results


def fix_multiline_words(raw_str, valid_words=ENGL_WORDS, stemmer=None,
                        remove_all_hyphens=True):
    """
    Apply experimental fix for detecting words that are split between
    two lines and return the resulting string. If no changes are made
    to the given string, return None.

    The idea is to detect words that have been split between two lines
    and move the complete word to one line. However, only words that
    are hyphenated will be detected by the fix.

    Example input string:
    'The following is just one example of the sit-
    uation that the function is intended to fix. It should work on
    proper nouns, such as Nguy-
    en, and hyphenated phrases, such as finely-
    tuned.'

    Ideal output string:
    'The following is just one example of the situation
    that the function is intended to fix. It should work on
    proper nouns, such as Nguyen,
    and hyphenated phrases, such as finely-tuned.'

    Note that if 'remove_all_hyphens' is True, hyphenated phrases that
    are split on the hyphen will end up without a hyphen in them. For
    example, 'finely-tuned' as shown in the example above will instead
    end up being 'finelytuned.'

    :param raw_str: The string to be fixed
    :type raw_str: str
    :param valid_words: The set of words that are considered valid in
        the context of the data - if the experimental fix is applied,
        this list of words will be used to try to determine whether the
        hyphen should be removed
    :type valid_words: list(str)
    :param stemmer: An NLTK stemmer that can be used to find the stems
        of words
    :type stemmer: NLTK stemmer
    :param remove_all_hyphens: Whether to remove all hyphens or use the
        experimental conditions that attempt to only remove
        unnecessary hyphens
    :type remove_all_hyphens: bool
    :return: A 'fixed' copy of the given string or None if the fix was
        not applied
    :rtype: str/None
    """
    data_split = raw_str.splitlines()
    data_split = list(filter(
        lambda x: len(x) > 0,
        data_split
    ))

    data_changed = False

    if stemmer is None:
        stemmer = PorterStemmer()

    for i in range(len(data_split) - 1):
        """
        Some lines will end up as empty strings after the fix is applied, so we
        just need to make sure we skip those lines.
        """
        if len(data_split[i]) < 1:
            continue

        if data_split[i][-1] == '-':
            end_index = data_split[i + 1].find(' ')

            if end_index == -1:
                end_index = len(data_split[i + 1])

            if remove_all_hyphens:
                data_split[i] = '{word_1st_half}{word_2nd_half}'.format(
                    word_1st_half=data_split[i][:-1],
                    word_2nd_half=data_split[i + 1][:end_index]
                )
            else:  # apply experimental 'smart' fix
                start_index = data_split[i].rfind(' ') + 1
                part_1 = data_split[i][start_index:-1]  # leaves off hyphen
                part_2 = data_split[i + 1][:end_index]

                # define booleans for detecting split words
                is_split_word = stemmer.stem(part_1 + part_2) in valid_words
                is_proper_noun = (
                    part_1[0].isupper() and (
                        stemmer.stem(part_2) not in valid_words or
                        stemmer.stem(part_1) not in valid_words
                    )
                )

                # use booleans to decide how to deal with the split word
                if is_split_word or is_proper_noun:
                    # add full, non-hyphenated word to end of line it started on
                    data_split[i] = '{word_1st_half}{word_2nd_half}'.format(
                        word_1st_half=data_split[i][:-1],
                        word_2nd_half=data_split[i + 1][:end_index]
                    )
                else:  # full word should be hyphenated still
                    # join parts, but keep hyphen (e.g. 'finely-tuned')
                    data_split[i] += data_split[i + 1][:end_index]

            """
            Currently, both parts of the word are moved to the first line
            for all cases, so the second part of the word needs to be
            removed from the second line no matter what.
            """
            data_split[i + 1] = data_split[i + 1][end_index:]

            data_changed = True

    return '\n'.join(data_split)


def chunk_word_patterns_in_sentences(tagged_sentences,
                                     parsing_fcn=nltk.ne_chunk,
                                     chunk_tags=('PERSON', 'ORGANIZATION')):
    """
    Given a list of pos-tagged sentences, use an optionally specified
    parsing function to extract particular phrases and word patterns
    and return them.

    Note that if multiple chunk tags are specified, chunks that match
    the pattern(s) for more than one tag will only be assigned to the
    first tag that was parsed.

    If no parsing function is specified, NLTK's default named entity
    chunker is used. This function is a modified version of one found
    at https://github.com/lukewrites/NP_chunking_with_nltk

    :param tagged_sentences: A list of lists of pos-tagged words, where
        the lists of words are each a complete sentence
    :type tagged_sentences: list(list(tuple(str, str)))
    :param parsing_fcn: A function that parses tagged words according
        to some pattern and returns a tree representation of the result
    :type parsing_fcn: function
    :param chunk_tags: All of the chunk tags that will be used to
        extract chunks
    :type chunk_tags: tuple(str)
    :return: A dictionary mapping indices of sentences to a dictionary
        that maps each of the given chunk tags to a list of the chunks
        found in the sentence with that tag - if no chunks are found in
        a sentence that match a given tag, the list will contain only
        'N/A'
    :rtype: dict(int: dict(str: list(str)))
    """
    chunks = {}

    for index, sentence in enumerate(tagged_sentences):
        chunk_tree = parsing_fcn(sentence)
        chunks[index] = {chunk_tag: [] for chunk_tag in chunk_tags}

        for subtree in chunk_tree.subtrees():
            if subtree.label() in chunk_tags:
                chunk = ' '.join(word for (word, tag) in subtree.leaves())
                chunk_tag = subtree.label()

                chunks[index][chunk_tag].append(chunk)

        for chunk_tag in chunk_tags:
            if len(chunks[index][chunk_tag]) == 0:
                chunks[index][chunk_tag].append('N/A')

    return chunks


def chunk_word_patterns(tagged_words, parsing_fcn=nltk.ne_chunk,
                        chunk_tags=('PERSON', 'ORGANIZATION')):
    """
    Given a list of pos-tagged words and a list of the same words
    without tags, use a parsing function to find and extract particular
    phrases. Return a dictionary of the phrases captured by the parsing
    function.

    Note that if multiple chunk tags are specified, chunks that match
    the pattern(s) for more than one tag will only be assigned to the
    first tag that was parsed.

    If no parsing function is specified, NLTK's default named entity
    chunker is used. This function is a modified version of one found
    at https://github.com/lukewrites/NP_chunking_with_nltk

    :param tagged_words: A list of pos-tagged words
    :type tagged_words: list(tuple(str, str))
    :param parsing_fcn: A function that parses tagged words according
        to some pattern and returns a tree representation of the result
    :type parsing_fcn: function
    :param chunk_tags: All of the chunk tags that will be used to
        determine which chunks to extract
    :type chunk_tags: tuple(str)
    :return: A dictionary mapping chunk labels to lists of chunks found
        in the given list of words - if no chunks were found that
        matched a particular tag's pattern, the list will contain only
        'N/A'
    :rtype: dict(str: list(str))
    """
    chunks = {}
    chunk_tree = parsing_fcn(tagged_words)

    for subtree in chunk_tree.subtrees():
        if subtree.label() in chunk_tags:
            chunk = ' '.join(word for (word, tag) in subtree.leaves())
            chunk_tag = subtree.label()

            if chunk_tag in chunks:
                chunks[chunk_tag].append(chunk)
            else:
                chunks[chunk_tag] = [chunk]

    for chunk_tag in chunk_tags:
        if chunk_tag not in chunks:
            chunks[chunk_tag] = ['N/A']

    return chunks

# Use predefined chunking to determine authors


def chunk_words_for_author(tagged_words, chunk_tag='PERSON', parsing_fcn=nltk.ne_chunk):
    chunks = {}
    chunk_tree = parsing_fcn(tagged_words)
    person = []
    name = ''

    for subtree in chunk_tree.subtrees():
        if subtree.label() == chunk_tag:
            for leaf in subtree.leaves():
                person.append(leaf)
                for part in person:
                    name += part + ''
                    if chunk_tag in chunks:
                        chunks[chunk_tag].append(name)
                name = ' '
            person = []
    return chunks

# num_words_extract=100, abstract_identifiers=() - other params


def section_extraction(words, num_words_extract=100, abstract_identifiers=()):
    """
    Given a list of words from a document, search for a header for an
    abstract and return at least the given number of words that follow
    the first abstract header that's found.

    If additional identifiers for an abstract are provided, prefer the
    given identifiers over the default set. If the minimum number of
    words to extract does not end with a complete sentence, subsequent
    words will be extracted until the end of a sentence (as marked by a
    period) is reached.

    :param words: A list of all of the words from a document
    :type words: list(str)
    :param num_words_extract: The minimum number of words following the
        first abstract identifier that should be returned
    :type num_words_extract: int
    :param abstract_identifiers: Words or phrases that may precede the
        abstract
    :type abstract_identifiers: tuple(str)
    :return: A string containing at least the given number of words, or
        'N/A' if the abstract could not be extracted
    :rtype: str
    """
    sections = []
    abstract = []
    introduction = []
    experimentation = []
    results = []
    conclusion = []
    acknowledgements = []
    references = []

    abst_flag = False
    intro_flag = False
    expr_flag = False
    res_flag = False
    con_flag = False
    akn_flag = False
    ref_flag = False

    words = list(filter(lambda w: is_not_gibberish(w), words))
    for word in words:
        if word.lower() == 'abstract':
            abst_flag = True
        elif abst_flag:
            abstract.append(word)
        if word.lower() == 'introduction':
            abst_flag = False
            intro_flag = True
        elif intro_flag:
            introduction.append(word)
        if word.lower() in ['experimentation', 'experimental']:
            abst_flag = False
            intro_flag = False
            expr_flag = True
        elif expr_flag:
            experimentation.append(word)
        if word.lower() in ['results', 'result']:
            abst_flag = False
            intro_flag = False
            expr_flag = False
            res_flag = True
        elif res_flag:
            results.append(word)
        if word.lower() == 'conclusion':
            abst_flag = False
            intro_flag = False
            expr_flag = False
            res_flag = False
            con_flag = True
        elif con_flag:
            conclusion.append(word)
        if word.lower() in ['acknowledgments', 'acknowledgment']:
            abst_flag = False
            intro_flag = False
            expr_flag = False
            res_flag = False
            akn_flag = True
        elif akn_flag:
            acknowledgements.append(word)
        if word.lower() in ['references', 'reference']:
            abst_flag = False
            intro_flag = False
            expr_flag = False
            res_flag = False
            akn_flag = False
            ref_flag = True
        elif ref_flag:
            references.append(word)

    # in case there is no introduction heading to cut off the abstract, make a predefined cut off
    if not introduction:
        abstract.clear()

        for header in list(abstract_identifiers) + ['Abstract', 'ABSTRACT']:
            if header in words:
                start = words.index(header) + 1

                # Exclude colon if one is found after abstract identifier
                if words[start] == ':':
                    start += 1

                end = (
                    words[start + num_words_extract:].index('.') +
                    num_words_extract + start + 1
                )

                abstract = words[start:end]

    if len(abstract) == 0 and len(words) > 0:
        count = 0
        while count < len(words) - 1:
            if words[count].isalpha():
                break
            else:
                count += 1
        abstract = words[count:count + 100]

    sections.append(abstract)
    sections.append(introduction)
    sections.append(experimentation)
    sections.append(results)
    sections.append(conclusion)
    sections.append(acknowledgements)
    sections.append(references)

    return sections


"""
    for header in list(abstract_identifiers) + ['Abstract', 'ABSTRACT']:
        if header in words:
            start = words.index(header) + 1

            # Exclude colon if one is found after abstract identifier
            if words[start] == ':':
                start += 1


            Grab only the words that come after the abstract identifier and try
            to make sure the last sentence is not cut off by the word limit.

            end = (
                words[start + num_words_extract:].index('.') +
                num_words_extract + start + 1
            )

            abstract = words[start:end]

            # group punctuation correctly (experimental)
            strip_leading_space = [
                '.',
                ',',
                '?',
                '!',
                ':',
                ';',
                ')',
                ']',
                '}'
            ]
            strip_trailing_space = [
                '(',
                '[',
                '{'
            ]

            for index, word in enumerate(abstract):
                if word[0] == "'" or word in strip_leading_space:
                    abstract[index - 1] += word
                    abstract.pop(index)
                elif word in strip_trailing_space:
                    abstract[index] += abstract[index + 1]
                    abstract.pop(index)

            return ' '.join(abstract)

    return 'N/A'
"""


# TODO: handle case where gazetteer file isn't read correctly
# (file doesn't exist, isn't formatted correctly, etc.)
def gazetteer_matches(sentences, gazetteer='gazetteer.txt'):
    """
    Given a list of sentences and a key for matches found, search each
    sentence for the keywords from a gazetteer and return the results.

    :param sentences: All of the sentences that will be searched for
        keywords
    :type sentences: list(str)
    :param gazetteer: An absolute or relative path to a gazetteer,
        which should contain one keyword per line
    :type gazetteer: str
    :return: A dictionary mapping the indices of each sentence to a
        list of all of the keywords found in it - if no keywords were
        found in a sentence, the list is empty
    :rtype: dict(int: list(str))
    """
    # import keywords from gazetteer (file containing keywords)
    with open(os.path.abspath(gazetteer), 'r') as fgaz:
        gazetteer_keywords = fgaz.read().split('\n')

    gaz_matches = {}

    # find and store sentences containing words from the gazetteer
    for index, sentence in enumerate(sentences):
        sentence_words = nltk.tokenize.word_tokenize(sentence)
        gaz_matches_in_sentence = []

        for word in sentence_words:
            if word in gazetteer_keywords or word.lower() in gazetteer_keywords:
                gaz_matches_in_sentence.append(word)

        gaz_matches[index] = gaz_matches_in_sentence

    return gaz_matches

from engine.extractor import extract_pdf_text
import os


# TODO: update to work with new extraction
def labeled_files(root_dir):
    """
    Given the absolute path to a directory containing multiple
    subdirectories whose names are labels for the documents contained
    within, return a dictionary that maps labels to a list of paths to
    the respective documents each label applies to.

    :param root_dir: The absolute path to a directory containing 
        one or more subdirectories whose names are labels for the
        documents contained within
    :type root_dir: str
    :return: A dictionary mapping labels to paths to the documents the
        labels apply to
    :rtype: dict(str: list(str))
    """
    file_labels = {}

    path, labels = list(os.walk(root_dir))[0][:2]

    for label in labels:
        label_path = os.path.join(path, label)
        files = list(os.walk(label_path))[0][2]
        file_labels[label] = list(
            map(lambda x: os.path.join(label_path, x), files)
        )

    return file_labels


def document_labels(file_labels):
    """
    Given a dictionary mapping labels to paths to documents, return a
    dictionary that maps labels to the contents of each of the 
    documents.

    :param file_labels: A dictionary mapping labels to lists of paths 
        to documents
    :type file_labels: dict(str: list(str))
    :return: A dictionary mapping the labels from the given dictionary
        to lists of the contents of the documents from the given 
        dictionary
    :rtype: dict(str: list(str))
    """
    
    data_labels = {}

    for label, fpaths in file_labels.items():
        # optional print
        print('Label:', label)

        pdf_texts = []

        for fpath in fpaths:
            # optional print
            print('File:', fpath)

            with open(fpath, 'rb') as fp:
                pdf_texts.append(extract_pdf_text(fp))

                # optional print
                print('file appended successfully')

        data_labels[label] = pdf_texts

        # optional print
        print('pdf texts successfully saved')
        print('Texts:', data_labels[label])

    return data_labels

# TODO: pickle the classifier
# http://scikit-learn.org/stable/tutorial/basic/tutorial.html#model-persistence

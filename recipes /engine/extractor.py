import sys
import os
from shutil import copy, rmtree, copyfile
from helpers import get_id, file_list, get_config
from constants import *


def prepare_output_structure(func):
    """
    TODO: docstring
    :param func:
    :return:
    """

    def inner(*args, **kwargs):
        """
        TODO: docstring
        :param args:
        :param kwargs:
        :return:
        """
        if len(args) > 0:
            (extractor_path, input_path, output_path, recursive) = (
                args[0],
                args[1],
                os.path.abspath(args[2]),
                args[3]
            )
        else:
            (extractor_path, input_path, output_path, recursive) = (
                kwargs['extractor_path'],
                kwargs['input_path'],
                os.path.abspath(kwargs['output_path']),
                kwargs['recursive']
            )
        cfg = get_config()
        input_path = cfg[INP_ITEMS_KEY][0][INP_KEY]

        files_to_process = file_list(input_path, recursive)
        file_path_ids = {}
        for file_path in files_to_process:
            if kwargs['make_file_ids']:
                file_id = get_id(file_path)
                # file_name = '{file_id}.{ext}'.format(file_id=file_id, ext='pdf')
                file_name = '{file_id}.{ext}'.format(file_id=file_id, ext='pdf')
            else:
                relevant_part = file_path.split('/')[-1]
                if len(relevant_part) != 64:
                    file_id = get_id(file_path)
                    file_name = '{file_id}.{ext}'.format(file_id=file_id, ext='pdf')
                else:
                    file_id = os.path.basename(file_path)
                    file_name = '{file_id}'.format(file_id=file_id)
            original = '{}/{}'.format(output_path, file_name.replace('.pdf', ''))
            # save the corresponding id
            file_path_ids[file_id] = file_path

            # prepare the output directory for the current file
            file_output_path = os.path.join(output_path, original)
            try:
                os.makedirs(file_output_path)
            except:
                pass

            # copy the original file into the folder with the structure
            original = '{}/{}'.format(input_path, file_name.replace('.pdf', ''))
            print('Copying file: {}'.format(original))
            copyfile(file_path, os.path.join(file_output_path, file_name))
            os.system('mv {} {}'.format(file_path, original))

            # TODO: add temp output dir so extraction isn't run multiple times

        # run the extraction function
        func(extractor_path, output_path, output_path, True)

        return file_path_ids

    return inner


def extract_single_pdf_contents(extract_fcn):
    """
    Given a PDF extraction function, return a new function that will
    correctly extract the contents of a single PDF document.

    :param extract_fcn: A function that uses the Java PDF tools to
        extract images or text from a given PDF document
    :type extract_fcn: function
    :return: A new function that, if given a path to a single PDF
        document, will correctly extract text or images from it
    :rtype: function
    """

    def inner(*args, **kwargs):
        """
        Given all of the arguments that are passed to the extraction
        functions, make all of the paths absolute and handle the
        preparation and cleanup for extracting the contents of a single
        PDF document.

        :param args: All of the arguments that are passed to the
            extraction functions
        :return: The return value of the original extraction function
        """
        if len(args) > 0:
            try:
                extractor_path, input_path, output_path, _ = args
            except:
                extractor_path, input_path, output_path = args
        else:
            (extractor_path, input_path, output_path) = (
                kwargs['extractor_path'],
                kwargs['input_path'],
                kwargs['output_path']
            )

        # convert relative paths into absolute ones
        extractor_path = os.path.abspath(extractor_path)
        input_path = os.path.abspath(input_path)
        output_path = os.path.abspath(output_path)

        if os.path.isdir(input_path):
            return extract_fcn(*args)
        else:
            # get input directory and name of input file
            input_dir, input_fname = os.path.split(input_path)

            # generate a unique name for a temporary input directory
            tmp_dir_name = get_id(input_fname)
            tmp_dir_path = os.path.join(
                input_dir,
                tmp_dir_name
            )

            """
            The Java PDF tools currently only work if they are fed input
            directories: they break if the given input path is to a file. Our
            workaround is to create a temporary input directory with a unique
            name (generated using timestamps and hashing, very fancy stuff),
            copy the PDF into that directory, extract it like normal, and then
            delete the temporary directory with the file in it. We live to
            please.
            """

            # create the temporary input directory and copy the file into it
            os.mkdir(tmp_dir_path)
            copy(input_path, tmp_dir_path)

            # call the extraction function
            results = extract_fcn(
                extractor_path,
                tmp_dir_path,
                output_path,
                False
            )

            # delete the temporary input directory
            rmtree(tmp_dir_path)

            return results

    return inner


def extract_pdf_contents(extractor_path, input_path, output_path,
                         recursive=False):
    """
    Given the path to the extraction tools, a path to an input
    directory, a path to an output directory, and a boolean indicating
    whether to parse the contents of the given input directory
    recursively, call the Java PDF tools to extract both the text and
    images from the PDFs in the given input directory.

    Currently, the text extraction tool gives the output text files the
    same names as their respective input PDFs. The image extraction
    tool creates a folder for every file in the given input directory
    to hold the images extracted from the respective PDFs. The folders
    have the same name as the PDF the images were extracted from.

    Both tools replicate the file structure of the given input
    directory in the given output directory.

    :param extractor_path: An absolute or relative path to the
        extraction tools
    :type extractor_path: str
    :param input_path: An absolute or relative path to an existing
        directory
    :type input_path: str
    :param output_path: An absolute or relative path to an output
        directory - if the given output directory does not already
        exist, it is created
    :type output_path: str
    :param recursive: Whether to recursively search the given input
        directory for PDFs to parse
    :type recursive: bool
    """
    extract_pdf_text(extractor_path, input_path, output_path, recursive)
    extract_pdf_images(extractor_path, input_path, output_path, recursive)


@extract_single_pdf_contents
def extract_pdf_text(extractor_path, input_path, output_path, recursive=False):
    """
    Given the path to the text extractor, a path to an input directory,
    a path to an output directory, and a boolean indicating whether to
    parse the contents of the given input directory recursively, call
    the Java PDF tool to extract the text from the PDFs in the given
    input directory and save them in text files in the given output
    directory.

    Currently, the Java PDF tool gives the output text files the same
    names as their respective input PDFs. It also replicates the file
    structure of the given input directory in the given output
    directory.

    :param extractor_path: An absolute or relative path to the
        text extractor
    :type extractor_path: str
    :param input_path: An absolute or relative path to an existing
        directory
    :type input_path: str
    :param output_path: An absolute or relative path to an output
        directory - if the given output directory does not already
        exist, it is created
    :type output_path: str
    :param recursive: Whether to recursively search the given input
        directory for PDFs to parse
    :type recursive: bool
    """
    # convert relative paths into absolute ones
    input_path = os.path.abspath(input_path)
    output_path = os.path.abspath(output_path)

    tools_compiled = os.path.exists(os.path.join(
        extractor_path,
        'target',
        'classes'
    ))

    command = (
        'cd {tools_path}{compile} && mvn exec:java ' +
        '-Dexec.mainClass="gov.llnl.pdftools.PDFToText" ' +
        '-Dexec.args="{inp} {out} {recursive}"'
    ).format(
        tools_path=extractor_path,
        compile='' if tools_compiled else ' && mvn compile',
        inp=input_path,
        out=output_path,
        recursive='-r' if recursive else ''
    )

    print("Extracting Text.......")
    
    # report the progress to the command line
    print(os.system(command))


@extract_single_pdf_contents
def extract_pdf_images(extractor_path, input_path, output_path,
                       recursive=False):
    """
    Given the path to the image extractor, a path to an input directory,
    a path to an output directory, and a boolean indicating whether to
    parse the contents of the given input directory recursively, call
    the Java PDF tool to extract images from the PDFs in the given
    input directory and save them in organized directories in the given
    output directory.

    Currently, the Java PDF tool creates a folder for every file in the
    given input directory to hold the images extracted from the
    respective PDFs. The folders have the same name as the PDF the
    images were extracted from. The tool also replicates the file
    structure of the given input directory in the given output
    directory.

    :param extractor_path: An absolute or relative path to the image
        extractor
    :param input_path: An absolute or relative path to an existing
        directory
    :type input_path: str
    :param output_path: An absolute or relative path to an output
        directory - if the given output directory does not already
        exist, it is created
    :type output_path: str
    :param recursive: Whether to recursively search the given input
        directory for PDFs to parse
    :type recursive: bool
    """

    # Reads all file names in directory and appends the name of the pdf folders to list pdf_list
    pdf_list = [item for item in os.listdir(input_path) if os.path.isdir(os.path.join(input_path, item))]
    # Will be the list for all the paths to pass to 'runExtractor()'
    list_to_run = []

    # Creates all the directory directions to be ran over by 'runExtractor()'
    for x in pdf_list:
        # Makes path to individual pdf
        pdf_path = '{}/{}/{}.pdf'.format(output_path, x, x)
        destination = '{}/{}/prefix'.format(output_path, x)
        list_to_run.append([pdf_path, destination])

    # Changes directory to where the extractor path is located
    os.chdir(extractor_path + '/pdffigures2')

    # Runs the extractor on all pdfs in list_to_run
    for x in list_to_run:
        # Creates the command line string to run the extractor
        cmd_str = 'sbt "run-main org.allenai.pdffigures2.FigureExtractorBatchCli ' \
                  + x[0] + ' -d ' + x[1] + ' -m ' + x[1] + '"'
        # Executes the command
        os.system(cmd_str)
    os.chdir('..')
    os.chdir('..')


def build_labels(input_path):
    input_path = os.path.abspath(input_path)
    materials = list(os.walk(input_path))
    # materials.remove('.DS_Store')
    print('materials')
    print(materials)

    result = {}
    for material in materials:
        result[material] = {}
        path, morphologies = list(os.walk(os.path.join(input_path, material)))[0][:2]
        print(path)
        print(morphologies)
        print('path')
        for morphology in morphologies:
            file_info = list(os.walk(os.path.join(path, morphology)))[0]
            morphology_path, pdfs = file_info[0], file_info[2]

            result[material][morphology] = list(map(lambda pdf: os.path.join(morphology_path, pdf), pdfs))
    print(result)

    # build the file info with the final breakdown 
    final_file_info = {}
    for material, morphologies in result.items():
        for morphology, files in morphologies.items():
            for current_file in files:
                final_file_info[current_file] = {
                    'material': material,
                    'morphology': morphology
                }
    return final_file_info


if __name__ == '__main__':
    extract_pdf_contents(*sys.argv[1:])
    # list1 = ["/llnl/recipes_extractor/pdf-tools","/llnl/documents" ,"/llnl/static" ]
    # extract_single_pdf_contents(list1) changed for images
    # extract_pdf_images(list1[0], list1[1], list1[2])

#!/usr/bin/python3

import sys
import os
from datetime import datetime


def get_choice(options, greeting='Options:',):
    """
    Given a list of all of the options, print a greeting and a numbered
    list of the given options, then prompt the user to pick from the
    list until a valid selection is received. Return the selection.

    :param options: A list of the options
    :type options: list(str)
    :param greeting: The message to be printed before the list of
        options
    :type greeting: str
    :return: A valid selection
    :rtype: int
    """
    num_options = len(options)

    print(greeting)

    menu = ['({option_num})  {option_name}'] * num_options

    for i in range(num_options):
        menu[i] = menu[i].format(
            option_num=(i + 1),
            option_name=options[i]
        )

    print('\n'.join(menu))

    u_in = validate_input(
        '\nInput choice: ',
        'Error: invalid choice.',
        tuple(str(x + 1) for x in range(num_options))
    )

    choice = int(u_in) - 1

    # optional print
    print(
        "Selection: {sel_num} ({sel_name})".format(
            sel_num=u_in,
            sel_name=options[choice]
        ),
        end='\n\n'
    )

    return choice


def try_parse_int(u_in, lb=0, ub=999):
    """
    Attempt to parse and validate integer input.

    Given a string representing input and a range that the input should
    fall between, attempt to parse the input string and return a tuple
    containing the input (as an int if parsed, as a string otherwise)
    and a boolean indicating the validity of the input. For invalid
    input, print an error message identifying the problem.

    :param u_in: Input from the user
    :type u_in: str
    :param lb: The lower bound for the integer, inclusive 
        (defaults to 0)
    :type lb: int
    :param ub: The upper bound for the integer, inclusive 
        (defaults to 999)
    :type ub: int
    :return: The input (int if valid, str otherwise) and whether the 
        input is valid
    :rtype: tuple(str or int, bool)
    """

    try:
        u_in = int(u_in)
        valid = (lb <= u_in <= ub)
    except ValueError:
        print('Error: input must be an integer.', end='\n\n')
        return u_in, False

    if not valid:
        print(
            'Error: input must be between {lower_bound} '
            'and {upper_bound}, inclusive'.format(
                lower_bound=lb,
                upper_bound=ub
            ),
            end='\n\n'
        )

    return u_in, valid


def is_valid_input(u_in, valid_in=('y', 'n'), case_sensitive=False):
    """
    Determine the validity of the given string input.

    Given a string representing the input, a tuple containing all valid
    values, and a boolean indicating whether or not the input is case
    sensitive, return a boolean representing the validity of the input.

    :param u_in: Input from the user
    :type u_in: str
    :param valid_in: All valid input
    :type valid_in: tuple(str)
    :param case_sensitive: Whether the input is case sensitive
    :type case_sensitive: bool
    :return: Whether the given input is valid
    :rtype: bool
    """

    if not case_sensitive:
        u_in = u_in.lower()
        valid_in = [inp.lower() for inp in valid_in]

    return u_in in valid_in


def yn_prompt(prompt):
    """
    Validate and return a boolean representing the user's answer to a
    given 'yes or no'-type question.

    Given a string representing the prompt message, show the message to
    the user until they answer either 'yes' or 'no.' Return a boolean
    representing their answer.

    :param prompt: Prompt message
    :type prompt: str
    :return: Whether the user answered 'yes'
    :rtype: bool
    """
    u_in = input(prompt)[0]

    while not is_valid_input(u_in):
        u_in = input(prompt)[0]

    return u_in in ['y', 'Y']


def validate_input(prompt, error_message='Error: invalid input',
                   valid_in=('y', 'n'), case_sensitive=False):
    """
    Print given prompt and obtain a valid response.

    Given a string representing the desired prompt message, a string
    containing the desired error message for invalid input, a tuple
    containing valid input values, and a boolean indicating whether
    or not the input is case sensitive, prompt the user for input until
    a valid response is received and then return that response. For
    invalid input, print the given error message identifying the
    problem before displaying the given prompt message again.

    :param prompt: Prompt message
    :type prompt: str
    :param error_message: Error message for invalid input
    :type error_message: str
    :param valid_in: All valid input
    :type valid_in: tuple(str)
    :param case_sensitive: Whether the input is case sensitive
    :type case_sensitive: bool
    :return: Valid user input
    :rtype: str
    """
    u_in = input(prompt)

    while not is_valid_input(u_in, valid_in, case_sensitive):
        print(error_message, end='\n\n')
        u_in = input(prompt)

    return u_in


def get_valid_input_path(alt_path_prompt=None, is_file_valid=True,
                         is_directory_valid=False, is_stdin_valid=False):
    """
    Given a set of booleans indicating what kind of input paths are
    allowed, display a prompt until a valid input path is received from
    the user.

    There is an optional parameter for an alternate prompt. If it is
    'None,' a default prompt is be used; otherwise, the given prompt is
    used.

    If all of the given booleans are set to 'False,' 'stdin' is
    returned.

    If standard in is indicated as a valid choice, then 'stdin' is
    returned if it is chosen.

    :param alt_path_prompt: (optional) Alternate prompt to display
    :type alt_path_prompt: str
    :param is_file_valid: Whether a path to a file is considered valid
    :type is_file_valid: bool
    :param is_directory_valid: Whether a path to a directory is
        considered valid
    :type is_directory_valid: bool
    :param is_stdin_valid: Whether standard input is considered valid
    :type is_stdin_valid: bool
    :return: A valid input path, or 'stdin' if standard in is allowed
    :rtype: str
    """
    if not is_file_valid and not is_directory_valid and not is_stdin_valid:
        return 'stdin'

    if alt_path_prompt is not None:
        path_prompt = alt_path_prompt

    f_or_dir = is_file_valid and is_directory_valid

    options = [
        'absolute path',
        'relative path'
    ]

    if is_stdin_valid:
        options.append('standard in')

    while True:
        u_choice = get_choice(
            greeting='Path options (choose one):',
            options=options
        )

        is_rel_path = (options[u_choice][0].lower() == 'r')

        if is_stdin_valid and options[u_choice] == 'standard in':
            return 'stdin'
        else:
            if alt_path_prompt is None:
                path_prompt = \
                    'Enter the {path_type} path to a {input_type}: '.format(
                         path_type='relative' if is_rel_path else 'absolute',
                         input_type='file or directory' if f_or_dir else (
                            'file' if is_file_valid else 'directory'
                         )
                    )

            u_in_path = input(path_prompt)

            if is_rel_path:
                u_in_path = os.path.abspath(u_in_path)

            is_valid_dir = is_directory_valid and os.path.isdir(u_in_path)
            is_valid_file = is_file_valid and os.path.isfile(u_in_path)

            if is_valid_dir or is_valid_file:
                break
            else:
                print("'{inp_path}' is not a valid input path.".format(
                    inp_path=u_in_path
                ))

    return u_in_path


def get_valid_output_dir(alt_path_prompt=None, create_if_dne=True,
                         default_path='output', default_path_is_relative=True,
                         is_stdout_valid=False):
    """
    Prompt the user for a path to an output directory until a valid one
    is received, then return it. A default prompt will be displayed when
    asking for a path unless an alternate prompt is given.

    Before asking the user to input a path, prompt them to indicate if
    the path is relative or absolute, or if they would like to use a
    default path (if one is provided) or standard output (if it is
    allowed). If standard output is allowed, 'stdout' is returned if it
    is chosen.

    If the user inputs an invalid path, display an error message and
    repeat all of the prompts until a valid path is received.

    :param alt_path_prompt: (optional) Alternate prompt to display
    :type alt_path_prompt: str
    :param create_if_dne: Whether to create the user-input directory if
        it does not already exist
    :type create_if_dne: bool
    :param default_path: A valid path to a directory which can be
        chosen at the first prompt - an option for a default path will
        not be provided if the value of the given default path is
        'None.'
    :type default_path: str
    :param default_path_is_relative: Whether the given default path is
        relative
    :type default_path_is_relative: bool
    :param is_stdout_valid: Whether standard out is a valid option for
        output
    :type is_stdout_valid: bool
    :return: A valid path to a directory
    :rtype: str
    """
    if alt_path_prompt is not None:
        path_prompt = alt_path_prompt

    if default_path_is_relative and default_path is not None:
        default_path = os.path.abspath(default_path)

    options = [
        'absolute path',
        'relative path'
    ]

    if default_path is not None:
        options.append('default path')

    if is_stdout_valid:
        options.append('standard out')

    while True:
        u_choice = get_choice(
            options=options,
            greeting='Path options (choose one):'
        )

        if options[u_choice] == 'standard out':
            return 'stdout'
        elif options[u_choice] == 'default path':
            # optional print
            print("Default path: '{default_path}'".format(
                default_path=default_path
            ))

            u_in_path = default_path
        else:
            is_rel_path = (options[u_choice][0].lower() == 'r')

            if alt_path_prompt is None:
                path_prompt = \
                    'Enter the {type} path to the output directory: '.format(
                       type='relative' if is_rel_path else 'absolute'
                    )

            u_in_path = input(path_prompt)

            if is_rel_path:
                u_in_path = os.path.abspath(u_in_path)

            if os.path.isdir(u_in_path):
                break
            elif create_if_dne:
                try:
                    os.makedirs(u_in_path)
                except:
                    print(
                        "There was an error while creating '{path}'.\n"
                        "Error: {err_msg}".format(
                            path=u_in_path,
                            err_msg=sys.exc_info()[0]
                        )
                    )
                else:
                    print("'{path}' successfully created.".format(
                            path=u_in_path
                        ),
                        sep=''
                    )

                    break
            else:
                print(
                    "'{path}' is not a valid directory.".format(
                        path=u_in_path
                    )
                )

    return u_in_path


def generate_valid_output_file_name(prefix='output', timestamp=False,
                                    ext='txt', default_path='output',
                                    default_path_is_relative=True,
                                    is_stdout_valid=False):
    """
    Given a prefix, an extension, and a boolean indicating whether a
    timestamp should be appended to the file name, prompt the user for
    a valid directory and return a full, valid path for an output file.

    If the output file already exists, prompt the user to add a
    timestamp to the end of the new output file's name. If the user
    declines to add a timestamp, prompt them to choose a different
    output directory and/or file name. If they decline to choose a
    different output path, warn them that the existing file will be
    affected.

    :param prefix: The first part of the output file name - if a
        timestamp is included in the file name, the prefix will precede
        it
    :type prefix: str
    :param timestamp: Whether a timestamp should be appended to the
        output file's name
    :type timestamp: bool
    :param ext: The extension for the output file, with or without the
        period
    :type ext: str
    :param default_path: A valid path to a directory
    :type default_path: str
    :param default_path_is_relative: Whether the given default path is
        relative
    :type default_path_is_relative: bool
    :param is_stdout_valid: Whether standard out is a valid option for
        output
    :type is_stdout_valid: bool
    :return: A valid path to an output file
    :rtype: str
    """
    complete = False
    ext = ext.replace('.', '')

    while not complete:
        out_path = get_valid_output_dir(
            alt_path_prompt='Choose a directory for the output file: ',
            create_if_dne=True,
            default_path=default_path,
            default_path_is_relative=default_path_is_relative,
            is_stdout_valid=is_stdout_valid
        )

        if timestamp:
            out_name = '{prefix}_{timestamp:%m-%d-%Y-(%H-%M-%S)}.{ext}'.format(
                prefix=prefix,
                timestamp=datetime.now(),
                ext=ext
            )
        else:
            out_name = '{prefix}.{ext}'.format(
                prefix=prefix,
                ext=ext
            )

        out_path_full = os.path.join(out_path, out_name)
        complete = not os.path.exists(out_path_full)

        while os.path.exists(out_path_full):
            print("'{output_path}' already exists.".format(
                output_path=out_path_full
            ))

            if yn_prompt('Would you like to add a timestamp '
                         'to the output file name? '):
                out_name = \
                    '{prefix}_{timestamp:%m-%d-%Y-(%H-%M-%S)}.{ext}'.format(
                        prefix=prefix,
                        timestamp=datetime.now(),
                        ext=ext
                    )

                out_path_full = os.path.join(out_path, out_name)
                complete = True

            elif yn_prompt('Would you like to choose '
                           'a different location?'):
                complete = False
                break
            else:
                print('The existing file will be affected.')
                complete = True
                break

    return out_path_full


def get_raw_string(termination_str=''):
    """
    Prompt the user for string input and return everything up to (but
    not including) the given termination string.

    :param termination_str: The sequence of characters that marks the
        end of string input (will not be included in returned data)
    :type termination_str: str
    :return: The input data
    :rtype: str
    """
    print('Type or paste the string below.')

    if termination_str == '':
        print('Enter a blank line to terminate input.')
    else:
        print("Enter '{termination_string}' to terminate input.".format(
            termination_string=termination_str
        ))

    u_in = input('Input string: ')
    str_data = []

    while u_in != termination_str:
        str_data.append(u_in)
        u_in = input('...  ')

    str_data = '\n'.join(str_data)

    return str_data.strip()

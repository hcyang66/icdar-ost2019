import imghdr
from PIL import Image
import shutil
import hashlib
import json
import subprocess
from engine.input_validation_ceh import *
from constants import *
import sys
from datetime import datetime
import os.path
import re


pattern = re.compile("""^[A-Za-z0-9 _.,!"'/$]*$""")

def run_command(command):
    """
    This function executes shell commands and returns the output line by
    line using an iterator.

    :param command: A shell command
    :type command: str
    :return: List generator object that returns output line by line
    :rtype: iterator
    """
    command = command.split()

    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )

    return iter(process.stdout.readline, b"")


def get_config():
    """
    Read from a config file or generate one from user input if a file
    is not provided.

    :return: A dictionary containing all of the values needed for
        program execution
    :rtype: dict(...)
    """
    cfg_file = sys.argv[1] if len(sys.argv) > 1 else None

    if cfg_file is not None:
        with open(cfg_file) as fp:
            return json.loads(fp.read())

    # TODO: test to make sure it gets correct user input
    # TODO: handle missing/conflicting settings (default vals for missing keys)
    user_cfg = {
        EXTR_LOC_KEY: get_valid_input_path(
            alt_path_prompt='Enter the path to the extraction tools: ',
            is_file_valid=False,
            is_directory_valid=True,
            is_stdin_valid=False
        ),
        INP_ITEMS_KEY: []
    }

    while True:
        user_cfg[INP_ITEMS_KEY].append(get_item_to_process())

        if not yn_prompt('Add another item to process? (Y/N): '):
            break

    return user_cfg


def get_item_to_process():
    """
    Prompt the user for information about a single item to process and
    return their valid input.

    :return: All of the valid information about a single item to
        process that the user gave
    :rtype: dict(str: str, str: bool)
    """
    user_inp_item = {
        INP_KEY: get_valid_input_path(
            alt_path_prompt='Enter the path to an input PDF/folder: ',
            is_file_valid=True,
            is_directory_valid=True,
            is_stdin_valid=False
        )
    }

    user_inp_item.update({
        RECURSE_KEY: yn_prompt(
            'Search input directory recursively? (Y/N): '
        ) if os.path.isdir(user_inp_item[INP_KEY]) else False,
        OUT_KEY: get_valid_output_dir(is_stdout_valid=False),
        OUT_DB_UPLOAD: yn_prompt('Upload output to database? (Y/N): '),
        OUT_PDF_KEY: yn_prompt(
            'Save a copy of the PDF(s) in the output? (Y/N): '
        ),
        EXTR_IMG_KEY: yn_prompt('Extract images from PDF(s)? (Y/N): '),
        EXTR_TXT_KEY: yn_prompt('Extract text from the PDF(s)? (Y/N): '),
    })

    user_inp_item[TRAIN_CLSSFR_KEY] = yn_prompt(
        'Would you like to train a classifier using the given input? (Y/N): '
    ) if os.path.isdir(user_inp_item[INP_KEY]) else False

    user_inp_item[ANALYZE_KEY] = yn_prompt(
        'Analyze extracted text(s)? (Y/N): '
    ) if user_inp_item[EXTR_TXT_KEY] else False

    user_inp_item[CLSSFY_KEY] = yn_prompt(
        'Classify extracted document(s)? (Y/N): '
    ) if user_inp_item[EXTR_TXT_KEY] and user_inp_item[ANALYZE_KEY] else False

    return user_inp_item


def get_id(fname):
    """
    Given a path to a file, generate and return a unique ID.

    :param fname: The name of or absolute path to a file
    :type fname: str
    :return: A unique ID for the file
    :rtype: str
    """
    # get a hashable name for the input
    return hashlib.sha256(
        '{name}_{timestamp:%m-%d-%Y-(%H-%M-%S)}'.format(
            name=fname,
            timestamp=datetime.now()
        ).encode()
    ).hexdigest()


def file_list(root, recursive=True, file_types=['.pdf']):
    """
    Given a path to a directory, return a list of absolute paths to all
    of the files found within.

    :param root: An absolute or relative path to a directory
    :type root: str
    :param recursive: Whether to search the given directory recursively
    :type recursive: bool
    :return: A list of absolute paths to all files contained within
        the given directory and all of its subdirectories
    :rtype: list(str)
    """
    if os.path.isfile(root):
        return [os.path.abspath(root)]

    fpaths = []

    for abs_path, dirs, fnames in os.walk(root):
        abs_path = os.path.abspath(abs_path)
        fpaths.extend(map(lambda x: os.path.join(abs_path, x),
                          filter(lambda name: os.path.splitext(name)[1] in file_types, fnames)))

        if not recursive:
            break

    return fpaths


def image_fnames(images_dir_path):
    """
    Given an absolute or relative path to a directory, return the
    names of all of the image files found within. If the directory does
    not exist or is empty, return an empty list.

    :param images_dir_path: An absolute or relative path to a directory
        containing image files
    :type images_dir_path: str
    :return: The names of all of the image files found within
    :rtype: list(str)
    """
    images_dir_path = os.path.abspath(images_dir_path)

    # get list of paths to image files, if the images exist
    if not os.path.isdir(images_dir_path):
        return []
    else:
        image_file_names = list(os.walk(images_dir_path))[0][2]

        return list(filter(
            lambda x: '.json' not in x and len(x) > 0,
            image_file_names
        ))


# def database_preparation(paper_data, paper_id, image_file_names, images_dir_path, material=None, morphology=None):
#     """
#     Given the results of analysis of a document, the ID of the
#     document, and a list of the names of all of the image files, return
#     the information that should be stored in the database in an
#     appropriate data structure.
#
#     :param paper_data: The results of running some form of analysis on
#         a document
#     :type paper_data: dict(...)
#     :param paper_id: The unique ID of the document
#     :type paper_id: str
#     :param image_file_names: The names of all of the image files that
#         were extracted from the document
#     :type image_file_names: list(str)
#     :return: All of the data from the document to be stored in a
#         database
#     :rtype: dict(...)
#     """
#     # filter images that 1) ARE in fact images and 2) are bigger than a predefined threshold
#     image_file_names = filter(lambda img: is_visual(img), image_file_names)
#
#     return {
#         'paper_id': paper_id,
#         'title': paper_data[SECTIONS_KEY][TITLE_KEY][0],
#         'morphology': morphology,
#         'material': material,
#         'images': list(map(
#             #lambda x: 'static/images/{paper_id}/{paper_id}/{img_name}'.format(
#             lambda x: '/Users/Carter/Desktop/output/{paper_id}/{img_name}'.format(
#                 paper_id=paper_id,
#                 img_name=x
#             ),
#             image_file_names
#         )),
#         'content': {
#             'abstract': paper_data[SECTIONS_KEY][ABSTRACT_KEY],
#             'introduction': paper_data[SECTIONS_KEY][INTRO_KEY],
#             'experimentation': paper_data[SECTIONS_KEY][EXPERIMENT_KEY],
#             'results': paper_data[SECTIONS_KEY][RESULTS_KEY],
#             'conclusion': paper_data[SECTIONS_KEY][CONCL_KEY],
#             'acknowledgements': paper_data[SECTIONS_KEY][ACKNOWLEDGE_KEY],
#             'references': paper_data[SECTIONS_KEY][REFERENCE_KEY],
#             'full_text': paper_data[TEXT_KEY]
#         },
#         'authors': paper_data[SECTIONS_KEY][AUTHORS_KEY]
#     }


def database_preparation(output_path, paper_id, file):
    json_path = output_path + '/' + file + '/' + file + '.json'
    image_json_path = output_path + '/' + file + '/prefix' + file + '.json'
    print(image_json_path)
    #somethimes file[:-4]
    with open(json_path) as data_file:
        data = json.load(data_file)
        # need morphology here
        authors = [author for author in data['multiline_word_fix_text']['extracted_sections']['authors']]
        abstract = ''.join(word + ' ' for word in data['multiline_word_fix_text']['extracted_sections']['abstract'])
        introduction = ''.join(word + ' ' for word in data['multiline_word_fix_text']['extracted_sections']['introduction'])
        experimentation = ''.join(word + ' ' for word in data['multiline_word_fix_text']['extracted_sections']['experimentation'])
        results = ''.join(word + ' ' for word in data['multiline_word_fix_text']['extracted_sections']['results'])
        conclusion = ''.join(word + ' ' for word in data['multiline_word_fix_text']['extracted_sections']['conclusion'])
        references = ''.join(word + ' ' for word in data['multiline_word_fix_text']['extracted_sections']['references'])
        full_text = data['multiline_word_fix_text']['text_plain']
        title = ''.join(word for word in data['multiline_word_fix_text']['extracted_sections']['title'])
        # need material here
        with open(image_json_path) as image_data_file:
            data = json.load(image_data_file)
            images = []
            for section in data:
                caption = section['caption']
                url = section['renderURL']
                images.append({'path': url, 'caption': caption})
    paper_item = ({
        'paper_id': paper_id,
        'authors': authors,
        'content': {
            'abstract': abstract,
            'introduction': introduction,
            'experimentation': experimentation,
            'results': results,
            'conclusion': conclusion,
            'references': references,
            'full_text': full_text
        },
        'title': title,
        'images': images
    })
    print('here')
    print(paper_item)
    return(paper_item)


def read_gazetteer_to_list(full_path=''):
    vocabulary = []
    with open(full_path) as g:
        for line in g:
            vocabulary.append(line.strip())
    return vocabulary


def is_visual(file_path):
    """checks if the image is visual by 
    testing agaisnt the given threshold

    :file_path: TODO
    :returns: TODO

    """
    with open(file_path, 'rb') as img_file:
        img_extension = imghdr.what(img_file)
        if not img_extension: 
            return False 
    width, height = Image.open(file_path).size 
    return width > MIN_WIDTH and height > MIN_HEIGHT

def is_not_gibberish(word):
    """checks if word contains only alpha-numeric symbols

    :word: TODO
    :returns: TODO

    """
    return pattern.search(word) 


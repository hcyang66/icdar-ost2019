import math
import fitz
from textblob import TextBlob as tb

def extract_top_words(pdf_list):
    def tf(word, blob):
        return blob.words.count(word) / len(blob.words)

    def n_containing(word, bloblist):
        return sum(1 for blob in bloblist if word in blob.words)

    def idf(word, bloblist):
        return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

    def tfidf(word, blob, bloblist):
        return tf(word, blob) * idf(word, bloblist)


    bloblist = []
    key_words = []

    for pdf in pdf_list:
        doc = fitz.open(pdf)
        text = ''
        for i in range(doc.pageCount):
            page = doc.loadPage(i)
            text += page.getText()
        bloblist.append(tb(text))

    for i, blob in enumerate(bloblist):
        scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        words = []
        print('doc')
        for word, score in sorted_words[:10]:
            words.append(word)
        key_words.append(words)
    print(key_words)
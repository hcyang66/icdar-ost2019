from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
import nltk
import string
import helpers
from sklearn import metrics


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems

def clean(documents):
    result = {}
    i = 0
    for path in documents:
        try:
            with open(path) as d:
                print(d)
                result[i] = d.read().lower().translate(string.punctuation)
                i += 1
        except:
            print("Cannot decode: {}".format(path))
    return result


stemmer = PorterStemmer()
training_documents = helpers.file_list('/home/sneha/Developer/recipes/trainingdocs', recursive=True, file_types=['.txt'])


# the labels for each of the training documents
training_target = [0, 1, 1]
testing_documents = helpers.file_list('/home/sneha/Developer/recipes/testingdocs', recursive=True, file_types=['.txt'])
testing_target = [0, 1, 1, 1]

"""
Define the vectorizer.
This will be how we generate the features for the classifier.
We could change the tokenize function to remove stop words using NLTK
if the scikit stopwords are not sufficient.
The vocabulary parameter are the words we are looking for.
Currently, these are used as features.
Other parameters to this function can be adjusted as needed.
"""
tfidf = TfidfVectorizer(
    tokenizer=tokenize,
    min_df=1,
    stop_words='english',
    vocabulary=helpers.read_gazetteer_to_list('../gazetteer_basic.txt')
)

print("____tfidf matrix_____")

# this is some sample code for
# if we just want to generate the TF-IDF for all documents without classifying
result = tfidf.fit_transform(
    clean(training_documents + testing_documents).values()
)

print(result)

feature_names = tfidf.get_feature_names()

print(result.toarray().tolist())
print("______classify______")

tfidf = TfidfVectorizer(
    tokenizer=tokenize,
    min_df=1,
    stop_words='english',
    vocabulary=helpers.read_gazetteer_to_list('../gazetteer.txt')
)

# create a pipeline with our feature generator and the classifier we want to use
text_clf = Pipeline([
    ('tfidf', tfidf),
    ('clf', LogisticRegression())
])

# train the classifier using the
text_clf = text_clf.fit(
    clean(training_documents).values(),
    training_target
)
predicted = text_clf.predict(
    clean(testing_documents).values()
)

# print(np.mean(predicted == testing_target))
print(metrics.classification_report(
    testing_target,
    predicted,
    target_names=['label 0', 'label 1'])
)

# TODO: pickle the classifier (save it for later usage)

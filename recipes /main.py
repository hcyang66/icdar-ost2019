from pymongo import MongoClient
from engine import analyzer, extractor
from metadata_extractor import content_extraction
import helpers
from constants import *
import os
import json


# always keep the connection open
connection = MongoClient('mongodb://localhost:27017/')
db = connection.live


# TODO: actually remove PDF from output if specified
# TODO (details below) handle conflicting configuration settings
# TODO sort out details of classification (default classifier, where/how to save newly trained classifier, etc.)
"""
Add checks for conflicting configuration settings. For example, you can't train
the classifier if the input is just a single PDF. You also can't run analysis if
text is not extracted from the PDF.
"""


def main():
    """
    Output the results of information extraction and analysis on any
    number of PDFs.

    If a JSON-formatted config file is provided as a command-line
    argument, all parameters for running the program will be extracted
    from it for automated information extraction. If a file is not
    provided, then the user will be prompted for such values as paths
    to input files and an output path.
    """
    cfg = helpers.get_config()

    extractor_path = cfg[EXTR_LOC_KEY]
    gazetteer_path = cfg[GAZ_LOC_KEY]
    input_items = cfg[INP_ITEMS_KEY]

    for input_item in input_items:
        if input_item[EXTR_IMG_KEY] and input_item[EXTR_TXT_KEY]:
            extr_fcn = extractor.extract_pdf_contents
        elif input_item[EXTR_IMG_KEY]:
            extr_fcn = extractor.extract_pdf_images
        elif input_item[EXTR_TXT_KEY]:
            extr_fcn = extractor.extract_pdf_text
        if input_item[EXTR_TXT_KEY] or input_item[EXTR_IMG_KEY]:
            if not input_item[TRAIN_CLSSFR_KEY]:
                extr_fcn = extractor.prepare_output_structure(extr_fcn)

            filepath_ids = extr_fcn(
                extractor_path=extractor_path,
                input_path=input_item[INP_KEY],
                output_path=input_item[OUT_KEY],
                recursive=input_item[RECURSE_KEY],
                make_file_ids=input_item[FILE_IDS_KEY]
            )

            # detect if the input path has a 'labeled' structure
            labeled_path = input_item['labeled_path']
            if labeled_path and filepath_ids:
                print('extractor.build_labels(input_item[INP_KEY])')
                labels_info = extractor.build_labels(input_item[INP_KEY])
                print(labels_info)
                # printing out material/morphology info for each file
                for filepath_id in filepath_ids:
                    print(filepath_id, ':', filepath_ids[filepath_id])

            abs_out_path = os.path.abspath(input_item[OUT_KEY])
            item_ids = list(os.walk(abs_out_path))[0][1]

            for item_id in item_ids:
                template_file_path = os.path.join(
                    abs_out_path,
                    item_id,
                    '{fname}.{ext}'
                )

                # build path to file holding extracted text
                # txt_file_path = template_file_path.format(
                #     fname=item_id,
                #     ext='txt'
                # )
                txt_file_path = template_file_path.format(
                    fname=item_id.replace('.pdf', ''),
                    ext='txt'
                )

                # get a list of the names of the extracted image files
                if not input_item[EXTR_IMG_KEY]:
                    image_file_names = []
                else:
                    # build path to image directory
                    image_dir_path = os.path.join(
                        abs_out_path,
                        item_id,
                        item_id
                    )

                    image_file_names = helpers.image_fnames(image_dir_path)

                # analyze text and save results of analysis to a JSON file
                if input_item[ANALYZE_KEY]:
                    if os.path.isfile(txt_file_path):
                        with open(txt_file_path) as txt_file:
                            item_results = analyzer.analyze_data(
                                raw_data=txt_file.read().strip(),
                                gazetteer_path=gazetteer_path
                            )

                        # add the morphology and material
                        try:
                            if labeled_path:
                                print(item_id)
                                print('inside')
                                item_results.update(labels_info[filepath_ids[item_id]])
                        except:
                            print('error: 118')
                        print('outside')
                        json_file_path = template_file_path.format(
                            fname=item_id,
                            ext='json'
                        )
                        print('final')

                        output_to_json(item_results, json_file_path)
                    else:
                        # optional print
                        print("'{out_fpath}' does not exist; no analysis run.".format(
                            out_fpath=txt_file_path
                        ))


    # upload analysis results to database, if indicated
    if input_item[OUT_DB_UPLOAD]:
        upload_paper_to_database(input_item[INP_KEY], input_item[OUT_KEY])
    # set everything to false except anazlyze if you just want this
    if input_items[0][ANALYZE_KEY]:
        content_extraction.main(input_item[OUT_KEY])


# def upload_paper_to_database(item_data, item_id, image_file_names):
#     """
#     Upload select parts of the given information about a paper to the
#     database.
#
#     The authors of the paper are only added to the database if they are
#     not found there already.
#
#     :param item_data: The data and information extracted from a
#         paper during analysis
#     :type item_data: dict(...)
#     :param item_id: The ID for the paper
#     :type item_id: str
#     :param image_file_names: The names of all of the images that were
#         extracted from the paper
#     :type image_file_names: list(str)
#     """
#     # return a dictionary containing the info that belongs in the database
#     # print(item_id)
#     paper_item = helpers.database_preparation(
#         paper_data=item_data.get(
#             ML_WORDS_FIX_KEY,
#             item_data[RAW_KEY]
#         ),
#         material=item_data['material'],
#         morphology=item_data['morphology'],
#         paper_id=item_id,
#         image_file_names=image_file_names,
#         images_dir_path=image_dir_path
#     )
#     # print(paper_item)
#
#     author_ids = []
#
#     # add all the authors
#     for author in paper_item[AUTHORS_KEY]:
#         author_item = db.authors.find_one({'name': author})
#
#         if author_item is None:
#             author_id = db.authors.insert_one({'name': author}).inserted_id
#         else:
#             author_id = author_item.get('_id')
#
#         author_ids.append(author_id)
#
#     paper_item[AUTHORS_KEY] = author_ids
#
#     db.papers.insert_one(paper_item)


def upload_paper_to_database(input_path, output_path):
    files_to_upload = [item for item in os.listdir(input_path) if os.path.isfile(os.path.join(input_path, item))]
    for file in files_to_upload:
        try:
            paper_id = file.replace('.pdf', '')
            paper_item = helpers.database_preparation(output_path, paper_id, file.replace('.pdf', ''))
            author_ids = []
            for author in paper_item['authors']:
                author_item = db.authors.find_one({'name': author})
                if author_item is None:
                    author_id = db.authors.insert_one({'name': author}).inserted_id
                else:
                    author_id = author_item.get('_id')
                author_ids.append(author_id)
            paper_item['authors'] = author_ids
            if not db.papers.find_one({'paper_id': paper_id}):
                db.papers.insert_one(paper_item)
            else:
                print('already exists')
                db.papers.update_one(
                    {'paper_id': paper_item['paper_id']},
                    {
                        '$set': {
                            'authors': paper_item['authors'],
                            'content.Abstract' : paper_item['content']['abstract'],
                            'content.Introduction' : paper_item['content']['introduction'],
                            'content.Experimentation' : paper_item['content']['experimentation'],
                            'content.Results' : paper_item['content']['results'],
                            'content.Conclusion' : paper_item['content']['conclusion'],
                            'content.References' : paper_item['content']['references'],
                            'misc.full_text' : paper_item['content']['full_text'],
                            'title': paper_item['title'],
                            'images': paper_item['images']
                        }
                    }
                )
                print('updated')
        except:
            print('path upload error')
            print(paper_id)



def output_to_json(data_object, json_file_path):
    """
    Convert the given data object to a JSON string, write the string to
    the given JSON file, and return the absolute path to it.

    :param data_object: Data to be saved
    :param json_file_path: An absolute or relative path to a JSON file
    :type json_file_path: str
    :return: The absolute path to the JSON file
    :rtype: str
    """
    json_file_path = os.path.abspath(json_file_path)

    json_obj = json.dumps(
        data_object,
        indent=4,
        sort_keys=True,
        ensure_ascii=False
    )

    with open(json_file_path, 'w') as json_file:
        print(json_obj, file=json_file)

    return json_file_path


if __name__ == '__main__':
    main()

from metadata_extractor import content_extraction
import json
import shutil

def get_material(string):
    parts = string.split()
    if parts[0][0] == "P":
        return "palladium"
    elif parts[0][0] == "C":
        return "copper"
    else:
        if parts[0][1] == "g":
            return "silver"
        else:
            return "gold"


def extractor(path_to_pdfs):
    output_path = "OUTPUT"
    pdfs = content_extraction.file_list(path_to_pdfs)
    i_list = ["copyright", "journal", "publisher", "downloaded", "licensed", "download",
                         "doi", "ip", '■', 'XXX', 'org/', 'org']
    for doc in pdfs:
        name = doc[:-3] + "json"
        paper_id = doc.split('/')[-1].replace('.pdf', '')
        print(name)
        product = content_extraction.clean(doc, i_list)
        if product:
            p_dict = product['contents']
            product.update(p_dict)
            del product['contents']
            with open(output_path+"/"+paper_id+".json", "w") as f:
                f.write(json.dumps(product))


def temp():
    output_path = "OUTPUT"
    path_to_json = "/Users/caguirre97/developer/KDD/extractorannotator/relevant"
    path_to_p = "/Users/caguirre97/developer/KDD/batches/MATESC-Relevant"
    pdfs = content_extraction.file_list(path_to_json, True, ['.json'])
    i_list = ["copyright", "journal", "publisher", "downloaded", "licensed", "download",
              "doi", "ip", '■', 'XXX', 'org/', 'org']
    for doc in pdfs:
        name = doc[:-3] + ".json"
        paper_id = doc.split('/')[-1].replace('.json', '')
        print(name)
        pdf_path = path_to_p + "/c_batch/" + paper_id + ".pdf"
        print(pdf_path)
        product = content_extraction.clean(pdf_path, i_list)
        if product:
            p_dict = product['contents']
            product.update(p_dict)
            del product['contents']
            with open(output_path + "/" + paper_id + ".json", "w") as f:
                f.write(json.dumps(product))

def main():
    connection = content_extraction.MongoClient('mongodb://localhost:27017')
    db = connection.live

    doc_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/")
    counter = 1
    doi_dict = {}
    with open("testingdocs/articles-cameron-alyssa/grobid_extraction.json") as f:
        doi_dict = json.load(f)
    for doc in doc_list:
        paper_id = doc.split('/')[-1].replace('.pdf', '')
        print("{} out of {}".format(counter, len(doc_list)))
        parts = doc.split("/")
        material = parts[-3]
        morphology = parts[-2]
        product = {}
        material = get_material(material)  
        if material == "gold":
           product = content_extraction.clean(doc,
               ["copyright", "journal", "publisher", "downloaded", "licensed", "download",
        	"doi", "ip", '■', "XXX", "org/", "org"])
        if product:
            this_title = product['title']
            print("\t" + material)
            print("\t" + morphology)
            print("\t" + this_title)
            #print(product)
            doi = ''
            try:
                if doi_dict:
                    print(paper_id)
                doi = doi_dict[paper_id]['DOI']
                print("DOI is {}".format(doi))
            except:
                pass
            result = db.papers.update_one(
                {'title' : this_title},
                { 
                    '$set' : {
                        'material' : material,
                        'morphology' : morphology,
                        'DOI' : doi
                    }
                }
            )
            for doc in db.papers.find({'title': this_title}):
                print()
                print("\t\t***************FOUND**********\n")
            
        counter += 1

def make_test():
    ag_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Ag Papers")
    au_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Au Papers")
    cu_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Cu Papers")
    pd_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Pd Papers")
    master = [ag_list, au_list, cu_list, pd_list]
    i_list = ["copyright", "journal", "publisher", "downloaded", "licensed", "download",
                         "doi", "ip", '■', 'XXX', 'org/', 'org']
    doi_dict = {}
    with open("testingdocs/articles-cameron-alyssa/grobid_extraction.json") as f:
        doi_dict = json.load(f)
    output_path = "/llnl/recipes_extractor/metadata_extractor/new_json/"
    for list in master: 
        for file in list:
            name = file[:-3] + "json"
            paper_id = file.split('/')[-1].replace('.json', '')
            print(name)
            product = content_extraction.clean(file, i_list)		
            if product:
                try:
                    product['DOI'] = doi_dict[paper_id]['DOI']
                    print("DOI is {}".format(product['DOI']))
                except:
                    pass
                with open(output_path+paper_id+".json", "w") as f:
                    f.write(json.dumps(product))


def movef():
    ag_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Ag Papers", file_types=['.json'])
    au_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Au Papers", file_types=['.json'])
    cu_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Cu Papers", file_types=['.json'])
    pd_list = content_extraction.file_list("/llnl/recipes_extractor/testingdocs/articles-cameron-alyssa/Pd Papers", file_types=['.json'])
    for file in ag_list:
        parts = file.split("/")
        parts = parts[:-3] + ['extractor_output'] + parts[-3:]
        string = ""
        c = 0
        for p in parts:
            if not c:
                c += 1
            else:
                string += "/" + p
        shutil.copyfile(file, string)

#main()
#make_test()
# extractor("/Users/caguirre97/Downloads/UsedPDFS/relevantUsed")
temp()




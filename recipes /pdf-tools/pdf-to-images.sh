#!/bin/bash

if [[ $# < 2 ]] ; then 
	echo ""
	echo "ERROR: Incorrect usage."
	echo "Usage: pdf-to-text.sh <inDirectory> <outDirectory> [-r]"
	echo "[-r] option to enable recursive directory handling"
	echo ""
	exit 1
fi

className=gov.llnl.pdftools.PDFToImages
args="$@"

MAVEN_OPTS=-Xmx4g

if [ ! -e "target/classes" ] ; then
   mvn compile
fi

mvn exec:java -Dexec.mainClass="$className" -Dexec.args="$args"


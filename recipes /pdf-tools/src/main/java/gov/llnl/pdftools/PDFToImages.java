package gov.llnl.pdftools;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.FileVisitOption;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.TIFF;
import org.apache.tika.metadata.TikaCoreProperties;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;


public final class PDFToImages extends SimpleFileVisitor<Path> 
{
  private static final Tika tika = new Tika();
  private Path fromPath, toPath;

    public PDFToImages(Path fromPath, Path toPath) {
    	this.fromPath = fromPath;
    	this.toPath = toPath;
      tika.setMaxStringLength(-1);
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        Path targetPath = toPath.resolve(fromPath.relativize(dir));
        if(!Files.exists(targetPath)){
            Files.createDirectory(targetPath);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
    {
        try {

            String outFileName = toPath.resolve(fromPath.relativize(file)).toAbsolutePath().toString();
            int extensionNdx = outFileName.lastIndexOf(".");
            if (extensionNdx > 1) {
                final String finalName = outFileName.substring(0, extensionNdx);
                final String extension = outFileName.substring(extensionNdx);

                if (extension.toLowerCase().equals(".pdf")) {
                    File of = new File(finalName);
                    if ((of.exists() && of.isDirectory()) || of.mkdirs()) {
                        Path outFile = Paths.get(finalName);
                        convertPDF(file, outFile);
                        System.out.println("Extracted:(" + outFileName + ") to directory (" + finalName + ")");
                    }
                }
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return FileVisitResult.CONTINUE;
    }
    
    private void convertPDF(Path in, Path out) throws IOException
    {
      Gson gson = new Gson();
      
      PDDocument pdfDoc = null;
      PDFParser parser = null;
      InputStream pdfStream = null; 
      
      try {
        // Parse file using PDFBox
        pdfStream = new BufferedInputStream(new FileInputStream(in.toFile()));
        parser = new PDFParser(pdfStream);
        parser.parse();

        pdfDoc = parser.getPDDocument();
        if (pdfDoc == null) {
          throw new IOException("Parsed PDDocument is null.  See previous error");
        }

        // Loop over pages
        Iterator<?> iter = pdfDoc.getDocumentCatalog().getAllPages().iterator();
        int imageNumber = 1;
        int pageNumber = 1;
        while (iter.hasNext()) {
          PDPage page = (PDPage) iter.next();
          PDResources resources = page.getResources();

          // Loop over images on page
          Map<?,?> images = resources.getImages();
          if (images != null) {
            Iterator<?> imageIter = images.keySet().iterator();
            while (imageIter.hasNext()) {

              // Save each image
              String key = (String) imageIter.next();

              PDXObjectImage image = (PDXObjectImage) images.get(key);

              final String extension = "." + image.getSuffix();

              ByteArrayOutputStream os = new ByteArrayOutputStream();
              try {
                image.write2OutputStream(os);
                os.flush();

                byte[] data = os.toByteArray();
                if (data != null && data.length > 0) {

                  
                  // Extract and build Meta Data
                  Map<String, String> imageMeta = parseImageMeta(data);
                  imageMeta.put("FROM_FILE", in.toString());
                  imageMeta.put("FROM_PAGE", Integer.toString(pageNumber));

                  // Write Image File
                  String imageFileName = String.format("%s/img-%d%s", 
                      out.toAbsolutePath().toString(), imageNumber, extension);

                  com.google.common.io.Files.write(data, new File(imageFileName));

                  
                  // Write Meta File
                  String metaFileName = String.format("%s/img-%d.json",
                      out.toAbsolutePath().toString(), imageNumber);
                  
                  com.google.common.io.Files.write(gson.toJson(imageMeta).getBytes(), new File(metaFileName));                  
                  
                  ++imageNumber;
                }
              }
              finally {
                IOUtils.closeQuietly(os);
              }
            }
          }
          ++pageNumber;
        }
      } catch (Exception e) {
        e.printStackTrace();
      } 
      finally {
        if (pdfDoc != null) {
          try {
            pdfDoc.close();
          } catch (IOException e) {}
        }
        IOUtils.closeQuietly(pdfStream);
      }
    }
	
    
  public static final Property[] SAVE_IMAGE_PROPS = {TikaCoreProperties.TITLE, TikaCoreProperties.ALTITUDE,
      TikaCoreProperties.LATITUDE, TikaCoreProperties.LONGITUDE, TIFF.ORIGINAL_DATE, TikaCoreProperties.CREATED,
      TikaCoreProperties.MODIFIED, TikaCoreProperties.CREATOR, TikaCoreProperties.MODIFIER,
      TikaCoreProperties.COMMENTS, TikaCoreProperties.DESCRIPTION, TikaCoreProperties.KEYWORDS,
      TikaCoreProperties.SOURCE, TIFF.FLASH_FIRED, TIFF.FOCAL_LENGTH, TIFF.IMAGE_LENGTH, TIFF.IMAGE_WIDTH,
      TIFF.EXPOSURE_TIME, TIFF.ISO_SPEED_RATINGS, TIFF.EQUIPMENT_MAKE, TIFF.EQUIPMENT_MODEL, TIFF.BITS_PER_SAMPLE,
      TIFF.RESOLUTION_UNIT, TIFF.RESOLUTION_HORIZONTAL, TIFF.RESOLUTION_VERTICAL, TIFF.ORIENTATION,};

  /***
   * Parse out given binary image's meta data using Tika
   * 
   * @param data
   * @return
   * @throws IOException
   */
  public static Map<String, String> parseImageMeta(byte[] data) throws IOException {
    Metadata metadata = new Metadata();
    Map<String, String> imageMeta = Maps.newHashMap();

    InputStream is = null;
    
    try {
      
      is = new ByteArrayInputStream(data);

      tika.parse(is, metadata);

      for (Property prop : SAVE_IMAGE_PROPS) {
        final String metaKey = prop.getName().replaceAll(":", "_").toUpperCase();
        final String value = metadata.get(prop);
        if (value != null) {
          imageMeta.put(metaKey, value);
        }
      }
      imageMeta.put("Parser", tika.getClass().getName());
    }
    finally {
      IOUtils.closeQuietly(is);
    }
    return imageMeta;
  }

	public static void convertPDFS(Path inPath, Path outPath, boolean recursive) throws IOException
	{
		Set<FileVisitOption> options = Sets.newHashSet();

		// Validate inPath
		if (Files.notExists(inPath)) {
			throw new IOException("Given <inPath> ("+inPath.toFile().toString()+") does not exist");
		}
		if (! Files.isDirectory(inPath)) {
			throw new IOException("Given <inPath> ("+inPath.toFile().toString()+") is not a directory");
		}
		// Validate outPath
		if (Files.exists(outPath)) {
			if (! Files.isDirectory(outPath)) {
				throw new IOException("Existing <outPath> ("+outPath.toFile().toString()+") is not a directory");
			}
		}
		
		if (recursive)
			Files.walkFileTree(inPath, new PDFToImages(inPath, outPath));
		else
			Files.walkFileTree(inPath, options, 1, new PDFToImages(inPath, outPath));			
	}
	
	public static void main(String[] args) 
		throws IOException
	{
		
		Path inPath  = Paths.get(args[0]);
		Path outPath = Paths.get(args[1]);
		
		
		boolean recursive = false;
		if (args.length > 2)
			if (args[2].equalsIgnoreCase("-r")) 
				recursive=true;
		
		PDFToImages.convertPDFS(inPath, outPath, recursive);
	}
	
}

package gov.llnl.pdftools;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.FileVisitOption;
import java.util.Set;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;

import com.google.common.collect.Sets;


public final class PDFToText extends SimpleFileVisitor<Path>
{
	private static final Tika tika = new Tika();

	private Path fromPath, toPath;

	public PDFToText(Path fromPath, Path toPath) {
		this.fromPath = fromPath;
		this.toPath = toPath;
		tika.setMaxStringLength(-1);
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		Path targetPath = toPath.resolve(fromPath.relativize(dir));
		if(!Files.exists(targetPath)){
			Files.createDirectory(targetPath);
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
	{
		try {
			String outFileName = toPath.resolve(fromPath.relativize(file)).toAbsolutePath().toString();
			int extensionNdx = outFileName.lastIndexOf(".");
			if (extensionNdx > 1) {
				final String finalName = outFileName.substring(0, extensionNdx) + ".txt";
				final String extension = outFileName.substring(extensionNdx);

				if (extension.toLowerCase().equals(".pdf")) {
					Path outFile = Paths.get(finalName);
					convertPDF(file, outFile);
					System.out.println("Converted:(" + outFileName + ") to file(" + finalName + ")");
				}
			}
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
		return FileVisitResult.CONTINUE;
	}

	private void convertPDF(Path in, Path out) throws IOException
	{
		try {
			final String text = tika.parseToString(in.toFile());
			Files.write(out, text.getBytes());
		}
		catch (TikaException e) {
			throw new IOException(e);
		}
	}

	private static void convertPDFS(Path inPath, Path outPath, boolean recursive) throws IOException
	{
		Set<FileVisitOption> options = Sets.newHashSet();

		// Validate inPath
		if (Files.notExists(inPath)) {
			throw new IOException("Given <inPath> ("+inPath.toFile().toString()+") does not exist");
		}
		if (! Files.isDirectory(inPath)) {
			throw new IOException("Given <inPath> ("+inPath.toFile().toString()+") is not a directory");
		}
		// Validate outPath
		if (Files.exists(outPath)) {
			if (! Files.isDirectory(outPath)) {
				throw new IOException("Existing <outPath> ("+outPath.toFile().toString()+") is not a directory");
			}
		}

		if (recursive)
			Files.walkFileTree(inPath, new PDFToText(inPath, outPath));
		else
			Files.walkFileTree(inPath, options, 1, new PDFToText(inPath, outPath));
	}

	public static void main(String[] args)
			throws IOException
	{

		Path inPath  = Paths.get(args[0]);
		Path outPath = Paths.get(args[1]);


		boolean recursive = false;
		if (args.length > 2)
			if (args[2].equalsIgnoreCase("-r"))
				recursive=true;

		PDFToText.convertPDFS(inPath, outPath, recursive);
	}

}

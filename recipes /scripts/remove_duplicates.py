import os
import shutil
from pymongo import MongoClient

def delete_dups_mongo():
    connection = MongoClient('mongodb://localhost:27017/')
    db = connection.live
    papers = db.papers.find()
    papers_list = []
    for paper in papers:
        paper_id = paper['paper_id']
        papers_list.append(paper_id)
        if '.pdf' in paper_id:
            db.papers.remove({'paper_id' : paper_id})
    return papers_list


def delete_dups_output():
    output = '/llnl/static/images'
    directories = filter(os.path.isdir, [os.path.join(output, directory) for directory in os.listdir(output)])
    for directory in directories:
        if '.pdf' in directory:
            shutil.rmtree(directory)


def delete_extension_from_input():
    input_path = '/llnl/documents'
    files = filter(os.path.isfile, [os.path.join(input_path, f) for f in os.listdir(input_path)])
    files_list = []
    os.chdir(input_path)
    for f in files:
        files_list.append(f.split('/')[-1])
        if '.pdf' in f:
            command = 'find -type f -name "*.pdf" | while read f; do mv "$f" "${f%.pdf}"; done'
            os.system(command)
    return files_list


def delete_bad_json():
    output = '/llnl/static/images'
    directories = filter(os.path.isdir, [os.path.join(output, directory) for directory in os.listdir(output)])
    for directory in directories:
        files = filter(os.path.isfile, [os.path.join(directory, f) for f in os.listdir(directory)])
        for f in files:
            if '.pdf.json' in f:
                os.remove(f)


def delete_differences(different):
    connection = MongoClient('mongodb://localhost:27017/')
    db = connection.live
    for paper_id in different:
        db.papers.remove({'paper_id' : paper_id})


def compare(papers, files):
    different = []
    for paper_id in papers:
        if paper_id not in files:
            different.append(paper_id)
    return different


def remove_unused():
    input = '/llnl/static/images'
    connection = MongoClient('mongodb://localhost:27017/')
    db = connection.live
    papers = db.papers.find()
    ids = []
    for paper in papers:
        ids.append(paper['paper_id'])
    for directory in os.listdir(input):
        if directory not in ids:
            os.system('rm -rf {}/{}'.format(input, directory))


#papers = delete_dups_mongo()
#delete_dups_output()
#delete_bad_json()
#files = delete_extension_from_input()
#different = compare(papers, files)
#delete_differences(different)
#remove_unused()


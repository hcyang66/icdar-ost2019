import os
import json

INPUT_PATH = 'pdf_input'

materials = list(os.walk(INPUT_PATH))[0][1]

result = {}
for material in materials:
    result[material] = {}

    path, morphologies = list(os.walk(os.path.join(INPUT_PATH, material)))[0][:2]
    for morphology in morphologies:
        file_info = list(os.walk(os.path.join(path, morphology)))[0]
        morphology_path, pdfs = file_info[0], file_info[2]

        result[material][morphology] = list(map(lambda pdf: os.path.join(morphology_path, pdf), pdfs))

print(json.dumps(result, indent=4))

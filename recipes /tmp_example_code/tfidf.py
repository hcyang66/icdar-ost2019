from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
import nltk
import string
from sklearn import metrics
import helpers
from PyPDF2 import PdfFileReader
import ntpath
import os.path
from pymongo import MongoClient


client = MongoClient('mongodb://fingolfin.kdd.cs.ksu.edu:27017/') #connecting
db = client.llnl_pdfs #creating a database
pdfs = db.pdfs #creating collection of pdfs

def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems

def clean(documents):
    result = {}
    i = 0
    for path in documents:
        try:

            with open(path) as d:
                result[i] = d.read().lower().translate(string.punctuation)
                i += 1
        except:
            print("Cannot decode: {}".format(path))
    return result

stemmer = PorterStemmer()
#line = line.strip('\n')

def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

# sample training and testing sets.
# These are lists of documents (single strings)

training_documents = helpers.file_list('/Users/alicelam/Developer/recipes/input/extractedHan', recursive=True, file_types=['.txt'])


# the labels for each of the training documents
training_target = [0]

testing_documents = helpers.file_list('/Users/alicelam/Developer/tagger/carlos_batch/slow/ext_revelant', recursive=True, file_types=['.txt'])
    #tokenize(openText)


testing_target = [0,1]

"""
Define the vectorizer.
This will be how we generate the features for the classifier.
We could change the tokenize function to remove stop words using NLTK
if the scikit stopwords are not sufficient.
The vocabulary parameter are the words we are looking for.
Currently, these are used as features.
Other parameters to this function can be adjusted as needed.
"""
tfidf = TfidfVectorizer(
    tokenizer=tokenize,
    min_df=1,
    stop_words='english',
    vocabulary=['silver',
                'nanowires'

                ]
)

print("____tfidf matrix_____")



# this is some sample code for
# if we just want to generate the TF-IDF for all documents without classifying
result = tfidf.fit_transform(
    clean(training_documents + testing_documents).values()
)

result2 = tfidf.fit_transform(
    clean(testing_documents).values()
)

print(result2)

result3 = result2.toarray().tolist()

i = 0
for filename in testing_documents:
   head, tail = ntpath.split(filename)
   print(tail + str(result3[i]))
   i += 1



#print(result)


feature_names = tfidf.get_feature_names()

print(result2.toarray().tolist())
print("______classify______")

tfidf = TfidfVectorizer(
    tokenizer=tokenize,
    min_df=1,
    stop_words='english',
    vocabulary=['silver',
                'nanowires'
                ]
)


# create a pipeline with our feature generator and the classifier we want to use
text_clf = Pipeline([
    ('tfidf', tfidf),
    ('clf', LogisticRegression())
])

# train the classifier using the
text_clf = text_clf.fit(
    clean(training_documents).values(),
    training_target
)

predicted = text_clf.predict(
    clean(testing_documents).values()
)

# print(np.mean(predicted == testing_target))
print(metrics.classification_report(
    testing_target,
    predicted,
    target_names=['label 0', 'label 1'])
)

# TODO: pickle the classifier (save it for later usage)

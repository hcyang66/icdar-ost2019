
Research Article
Synthesis and Growth Mechanism of Silver Nanowires through
Different Mediated Agents (CuCl2 and NaCl) Polyol Process

Mohd Rafie Johan, Nurul Azri Khalisah Aznan, Soo Teng Yee, Ing Hong Ho,
SooWern Ooi, Noorsaiyyidah Darman Singho, and Fatihah Aplop

Nanomaterials Engineering Research Group, Advanced Materials Research Laboratory, Department of Mechanical Engineering,
University of Malaya, 50603 Kuala Lumpur, Malaysia

Correspondence should be addressed to Noorsaiyyidah Darman Singho; ieyda putri@um.edu.my

Received 6 February 2014; Revised 23 April 2014; Accepted 23 April 2014; Published 22 May 2014

Academic Editor: Zhenhui Kang

Copyright ? 2014 Mohd Rafie Johan et al. This is an open access article distributed under the Creative Commons Attribution
License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly
cited.

Silver nanowires (AgNWs) have been synthesized by polyol process through different mediated agents (CuCl
2
and NaCl). The

presence of cations and anions (Cu(II), Na+, and Cl?) has been shown to have a strong impact on the shape of silver nanostructures.
The field emission scanning electronmicroscopy (FESEM) and transmission electronmicroscopy (TEM) show uniform nanowires.
The UV-vis spectra show that plasmon peak indicated the formation of nanowires. The X-ray diffraction (XRD) pattern displayed
that final product was highly crystallized and pure. The growth mechanism of AgNWs was proposed.

1. Introduction

One-dimensional (1-D) metal nanostructures such as nano-
wires have attracted extensive attention due to their unique
magnetic, optical, and electronic properties compared to
zero-dimensional (0-D) nanostructures [1?6]. Among these
1-Dmetal nanostructures, silver nanowires (AgNWs) are par-
ticularly of interest because the bulk Ag exhibits the highest
electrical and thermal conductivity among all metals. There
are many applications where nanowires could be exploited to
greatly enhance the functionality of a material [7?9]. In these
regards, the synthesis of nanowires has attracted attention
from a broad range of researchers [10?14]. Over the last
decade, variousmethods had been used to synthesizeAgNWs
such as polyol process [12?14], wet chemical synthesis [15, 16],
hydrothermal method [17, 18], and ultraviolet irradiation
photoreduction techniques [19, 20]. Among these methods,
the polyol process is considered due to simple, effective, low
cost, and high yield. In the polyol process, an exotic reagent
leads to the formation of wire like structure. Xia and Sun [13]
have modified the polyol process by generating AgNWs with
diameters in the range of 30?50 nm. By controlling the
parameters such as reaction time, molar ratio between cap-
ping agent and metallic precursor, temperature, and addition

of control agent, a reasonable control growth of AgNWs may
be achieved. In this work, AgNWs were synthesized through
reducing silver nitrate (AgNO

3
) with 1,2-propanediol (Sam-

ple 1) and ethylene glycol (EG) (Samples 2 and 3) in the pres-
ence of polyvinylpyrrolidone (PVP) as the surfactant which
can direct the growth of AgNWs and protect them from
aggregation. The mediated agents such as CuCl

2
(Sample 2)

and NaCl (Sample 3) are added to facilitate the growth of
AgNWs. We believe Cu(II), Na+, and Cl? ions are necessary
for AgNWs production.

2. Experimental Method

Anhydrous 1,2-propandiol (99%), AgNO
3
(99%), and polyvi-

nylpyrrolidonewere purchased fromAcros. All the chemicals
were used as received without any further purification.
The first sample (Sample 1) is without any mediated agent.
10mL of 1,2-propanediol was added into a 50mL three-
necked flask at 170?C for 2 h. Then 0.5mL of 1,2-propanediol
solution of AgNO

3
(0.005M) was injected into the 1,2-

propanediol under vigorous magnetic stirring. Later, 3mL of
1,2-propanediol solution of AgNO

3
(0.1M) and 3mL of 1,2-

propanediol solution of PVP (0.45M) were added dropwise

Hindawi Publishing Corporation
Journal of Nanomaterials
Volume 2014, Article ID 105454, 7 pages
http://dx.doi.org/10.1155/2014/105454




2 Journal of Nanomaterials

simultaneously over a period of 5 minutes. The solution
immediately turned from colorless to light yellow. The reac-
tion was continued for 1 h and heated at 170?C for 30min. A
grey suspensionwas obtained and allowed to cool at the room
temperature. Then, the mixture was diluted by acetone,
centrifuged, washed by deionized water, and dried in a
vacuum for 24 hours at room temperature.

The second sample (Sample 2) is with CuCl
2
as the

mediated agent. Firstly, 5mL of ethylene glycol was added
into a beaker and heated for 1 hour using silicon oil bath
at 150?C. Then, 40?L of a 4mM CuCl

2
?H

2
O/ethylene glycol

was added into the solution, stirred, and allowed to heat for
15 minutes. After that, 1.5mL of 114mM PVP/ethylene glycol
was added into the beaker, followed by 1.5mL of 94mM
AgNO

3
/ethylene glycol. The color changed to yellow and

became brownish grey after AgNO
3
/ethylene glycol was

added. The solution was heated for another 1 hour. Then, the
solution was taken out and let to cool at room temperature.
The solution was centrifuged at 3000 rpm and 30min each
with acetone and deionized water. The final product was
preserved in deionized water until characterization.

The third sample (Sample 3) is with NaCl as the mediated
agent. 15mL of 0.36M of ethylene glycol (EG) solution of
polyvinylpyrrolidone (PVP) was heated and stirred for 15
minutes. It was then followed by microwave heating at 170?C
for an hour. Then, 20?L of 0.1M EG solution of NaCl and
20?L of 0.06MEG solution of AgNO

3
were injected into

this EG solution of PVP to produce AgCl as seeds. After
15 minutes of injection, 15mL of 0.06M of EG solution of
AgNO

3
was injected into mixture solution within 5 minutes.

The mixture solution was stirred to obtain a homogeneous
solution.The color ofmixture solution slowly changes to light
yellow. The mixture solution was then heated under
microwave irradiation with temperature maintained at 170?C
for approximately 30minutes. It is allowed to cool naturally to
room temperature and turn to gray color. The resulting
solution was washed several times using acetone and ethanol
and then dried in vacuum at 60?C.

The X-ray diffraction (XRD) patterns were recorded
using a Siemens D5000 X-ray diffractometer (Cu-K? radia-
tion, ? = 0.154 nm). The transmission electron microscopy
(TEM) images were taken on a Libra 120 model TEM
using accelerating voltage of 400 kV. Field emission scanning
electronmicroscopy (FE-SEM) imageswere takenusingZeiss
Auriga.TheUV-visible spectrum of the as-prepared products
was recorded on a Varian Cary 50UV-vis spectroscopy.

3. Results and Discussion

3.1. Mechanism for the Formation of AgNWs

3.1.1. Scheme 1 (Sample 1 without any Mediated Agent). The
formation of anisotropic AgNWs involves two steps. In the
first step, 1,2-propanediol was converted to propionaldehyde
at high temperature (170?C) as shown in (1). Then it reduces
Ag+ ions to Ag atoms as shown in (2). Consider

HOCH3CH2CHOH
170

?C
????? CH3CH2CHO +H2O (1)

2Ag+ + 2CH3CH2CHO ?? CH3CH2COOH3 + 2Ag + 4H
+

(2)

In the second step, AgNO
3
and PVP were added dropwise

to the reaction system allowing the nucleation and growth of
AgNWs.

Ag + (--CH2--CH--) (--CH2--CH--)

N N

2HC

2HC

2HC

2HC

C=O C=O

CH2 CH2

Ag

nn

(3)

Ag atoms are nucleated through the homogeneous nucleation
process.TheseAgNpswerewell dispersed because of the pres-
ence of a polymeric surfactant PVP that could be chemically
adsorbed onto the surfaces of Ag through O?Ag bonding (3).
PVP has an affinity toward many chemicals to form coordi-
native compounds due to the structure of polyvinyl skeleton
with strong polar group (pyrrolidone ring). In this case,
C=O polar groups were interacted with Ag+ ions and form
coordinating complex as shown in (3). When this dispersion
of AgNps was continuously heated at 170?C, the small
nanoparticles progressively disappeared to the benefit of
larger ones viaOstwald ripening process [21].The critical par-
ticle radius increased with temperature. As the reaction con-
tinued, the small AgNpswere no longer stable in solution, and
they started to dissolve and contribute to the growth of larger
ones. With the assistance of PVP, some of the larger nanopar-
ticles were able to grow into rod-shaped structures. The
growth process would continue until all the AgNps were
completely consumed and only nanowires survived.

3.1.2. Scheme 2 (Sample 2 with CuCl
2
Mediated Agent). In the

initial step, high temperature is crucial for the conversion of
ethylene glycol to glycolaldehyde as shown in

2HOCH2CH2OH +O2
150

?C
????? 2CH3CHO + 2H2O (4)

AgNps were formed by reducing Ag+ ions with glycolalde-
hyde as shown in

2CH3CHO + 2Ag
+
?? CH3CO?OCCH3 + Ag + 2H

+

(5)

It was found that a small amount of Cl? must be added to a
polyol synthesis to provide electrostatic stabilization for the
initially formed Ag seeds [4]:

2HOCH2CHO + CuCl2 ?? 2CH2COOH + Cu + 2HCl
(6)

Ag+ + Cl? ?? AgCl (7)

In addition to electrostatically stabilizing the initially
formed Ag seeds, the high Cl? concentrations during CuCl

2



Journal of Nanomaterials 3

mediated synthesis help reduce the concentration of free Ag+
ions in the solution through the formation of AgCl. Sub-
sequent, it will slowly release the Ag+ and subsequent slow
release of Ag+ effectively. These facilitate the high-yield
formation of the thermodynamically more stable multiply
twinned Ag seeds that are required for wire length. Valency
metal ions (Cu2+) were reduced by EG to low valence (Cu+)
which in turn reacted with and scavenged adsorbed atomic
oxygen from the surface of AgNps. Here, Cu2+ can remove
oxygen from the solvent which prevents twinned seeds
dissolved by oxidative etching and scavenging adsorbed
atomic oxygen from the surface of the Ag seeds, facilitating
multiply twinned seeds growth [12].

In the final step, AgNO
3
and PVP were added dropwise

to the reaction system allowing the nucleation and growth of
silver nanowires as shown in (3). AgNps were well dispersed
because of the presence of PVP, a polymeric surfactant that
could be chemically adsorbed onto the surfaces of Ag through
O?Ag bonding. As the reaction continued, the small Ag
nanoparticles were no longer stable in solution and they
started to dissolve and contribute to the growth of larger
ones. When multiple twinned form the nanoparticles during
a nuclei period, the PVP was bounded preferently on {100}
then {111} [22, 23]. This inhibited the growth along {111}
direction. So the growth took place only in the {110} direction
resulting in the fivefold twinned nanowires (Figure 1).

3.1.3. Scheme 3 (Sample 3 with NaCl Mediated Agent). Like
in Scheme 2, the formation of anisotropic AgNWs involves a
number of steps. The first two steps are similar to (4) and (5).
Like in Scheme 2, chloride ions were added (8) to stabilize
AgNps and prevented the growth of nanoparticles [10]. As a
result, nanoparticles that can grow will dissolve via Oswald
ripening:

2HOCH2CHO + 2NaCl ?? 2CH2COOH + 2Na
+
+ 2HCl?

(8)

Ag+ + Cl? ?? AgCl (9)

The high Cl? concentration during NaCl mediated syn-
thesis helps reduce the concentration of free Ag+ in the solu-
tion through the formation of AgCl (9). Subsequent, it will
slowly release the Ag+ and subsequent slow release of Ag+,
effectively.This facilitates the high yield formation of the ther-
modynamically stable multiply twinned Ag seeds. So, AgCl
precipitate that forms in the early stages of the reaction serves
as a seed for multitwin particles. The formed AgCl nanopar-
ticles can be reduced slowly and decreased reaction rate
makes anisotropic growth of Ag nanowires favorable [24].
Meanwhile, Na+ can remove oxygen from the solvent which
prevents twinned seeds dissolved by oxidative etching and
scavenging adsorbed atomic oxygen from the surface of the
silver seeds, facilitating multiply twinned seeds growth [12].
In the final step, AgNO

3
and PVP were added dropwise to

the recreation system allowing the nucleation and growth of
AgNWs as shown in (3).

With passivation of some facets of particles by PVP,
some nanoparticles can grow into multitwin particles. PVP is

{111}

{10
0}

Figure 1: Schematic of 5-fold twinned pentagonal nanowires con-
sisting of five elongated {100} facets and 10 {111} end facets.

Figure 2: FESEM image of AgNWs (without any mediated agent).

believed to passivate (100) faces of these multitwin particles
and leave (111) planes achieve for anisotropic growth at [110]
direction [13] (Figure 1). As the addition of Ag+ continues,
multitwin particles grow into Ag nanowires.

3.2. FESEM Analysis. Figure 2 shows the FESEM image of
AgNWs without any mediated agents. The image shows
straightness along the longitudinal axis, the level of perfec-
tion, and the copiousness in quantity that we could routinely
achieve using this synthetic approach. The presence of silver
nanoparticles (AgNps) is also evident in the figure indicating
that not all of AgNps are transformed into nanowires.

Figure 3 shows the FESEM image of AgNWs with CuCl
2

as the mediated agent. The image reveals that the product is
entirely composed of a large quantity of uniform nanowires
with a mean diameter of 65 nm.The high faces of PVP on all
faces of the seeds lead to anisotropic growth mode.

Figure 4 shows the FESEM image of AgNWswithNaCl as
the mediated agent.The image shows well-defined wires with
mean diameter of 80 nm.

3.3. TEM Analysis. Figure 5 shows the TEM image of indi-
vidual AgNWs without any mediated agents with 84 nm in
diameter and 1119 nm in length.

Figure 6 shows a TEM image of an individual AgNW
(CuCl

2
mediated agent) with 87 nm in diameter and 3 ?m



4 Journal of Nanomaterials

Figure 3: FESEM image of AgNWs (with CuCl
2
mediated agent).

Figure 4: FESEM image of AgNWs (with NaCl mediated agent).

in length. The insert of Figure 6 reveals a pentagonal cross-
section of AgNWs and indicates the multiple-twinned struc-
ture of the AgNWs [24].

Figure 7 shows the TEM image of AgNWs with NaCl
mediated agent. The TEM image displayed a twin boundary
in the middle of nanowires and pentagon shape cross-section
at the end of nanowires.

3.4. XRD Analysis. Figure 8 shows the XRD pattern of
AgNWswith andwithoutmediated agent.Thepeaks at angles
of 2? = 38.1?, 44.2?, 64.3?, and 77.5? correspond to the (111),
(200), (220), and (311) crystal planes of the face center cubic
(FCC) Ag, respectively. The lattice constant calculated from
the diffraction pattern was 0.4086 nm, which is in agreement
with the reported value of silver (JCPDS 04-0783). Further-
more, the intensity ratio of the (111) to (200) peaks is 2.0, in
good agreement with the theoretical ratio, that is, 2.5 [14].

Figure 9 shows the XRD pattern of highly crystalline
AgNWs with CuCl

2
mediated agent. It has a similar pattern

with AgNWs without mediated agent (Figure 8). The XRD
pattern reveals that the synthesis AgNWs through polyol
process comprise pure phase. The lattice constant, ?, was
4.07724 ?A? which is almost approaching the literature value of
4.086 ?A?. The ratio of intensity between (111) and (200) peaks
reveals a relatively high value of 3.2 compared to the theoret-
ical ratio value of 2.5. This high value of ratio indicates the
enchancement of {111} crystalline planes in the AgNWs.

Figure 10 shows the XRD pattern of AgNWs with NaCl
mediated agent. It exhibits well-defined peaks without any
impurity element peaks detected. This indicated the success

Figure 5: TEM image of AgNWs without any mediated agent.

Figure 6: TEM image ofAgNWswithCuCl
2
mediated agent (insert:

pentagon cross-section of AgNWs).

Figure 7: TEM image of AgNWs (with NaCl mediated agent).

0

20

40

60

80

100

120

140

160

20 25 30 35 40 45 50 55 60 65 70 75 80

In
te

ns
ity

(a
.u

.)

(200)
(220) (311)

(111)

2?

Figure 8: XRD pattern of AgNWs without any mediated agent.



Journal of Nanomaterials 5

0

50

100

150

200

250

300

350

400

30 40 50 60 70 80

In
te

ns
ity

(a
.u

.)

(200)

(220) (311)

(111)

2? (deg)

Figure 9: XRD pattern of AgNWs (with CuCl
2
mediated agent).

0

0.2

0.4

0.6

0.8

1

1.2

20 30 40 50 60 70 80

Co
un

ts
(s

)

(111)

(200)
(220) (311)

2?

Figure 10: XRD patterns of the as-synthesized AgNWs (with NaCl
mediated agent).

in the formation of the crystalline silver nanowires. The four
diffraction peaks obtained are similar to peaks in Figures 8
and 9. It is worth noting that the ratio of intensity between
(111) and (200) peaks exhibits a relative value of 2.05 (the
theoretical ratio is 2.5). It indicated that the sample becomes
less crystalline with NaCl mediated agent.

3.5. UV-Vis Spectroscopy Analysis. Figure 11 shows the absor-
ption spectra of AgNWs without mediated agent at different
molar ratio of PVP to AgNO

3
. The appearance of a weak

surface plasmon resonance (SPR) at 425 nm indicated the
formation of AgNps [17]. This implies that the final product
synthesized under this particular condition was a mixture of
AgNps and AgNWs. As the molar ratio increases from 4.5 to
7.5, the intensity of the SPR is slightly decreased. Furthermore,
the SPR peak around 425 nm is blue-shifted to 404 nm. The
shoulder peak at 380 nm could be considered as the optical
signature of AgNWs. At this point, optical signatures similar
to those of bulk Ag also began to appear as indicated by the

300.0 350.0 400.0 450.0 500.0 550.0 600.0

Ab
so

rb
an

ce
(a

.u
.)

Wavelength (nm)

?0.5

?0.4

?0.3

?0.2

?0.1

0.0

0.1

0.2

0.3

0.4

0.5

0.6

Molar ratio = 4.5
Molar ratio = 6
Molar ratio = 7.5

425nm

380nm

350nm

Figure 11: Absorption spectra of AgNWs (without any mediated
agent) with different molar ratio of PVP and AgNO

3
.

0

0.02

0.04

0.06

0.08

0.1

0.12

0.14

300 350 400 450 500 550 600

Ab
so

rb
an

ce
(a

.u
.)

Wavelength (nm)

Figure 12: Absorption spectra of AgNWs (with CuCl
2
mediated

agent).

shoulder peak around 350 nm. With increasing molar ratio,
the intensity of absorption bands at 350 and 380 nm increased
apparently due to increased density of AgNWs.

Figure 12 shows the UV-vis absorption spectra for
AgNWs with CuCl

2
mediated agent. The peak positions at

391 nm could be considered as the optical signature of rela-
tively long AgNWs. At this point, optical signatures similar to
those of bulk silver also began to appear as indicated by the
shoulder peak around 357 nm [25]. Compared to the previous
absorption spectra (Figure 11), no peak for AgNps existed.
This indicates that the sample is completely of AgNWs.

Figure 13 shows the absorption spectra of ANWs (with
NaCl mediated agent) synthesized with different molar of
AgNO

3
. The appearance of a story peak at 384 nm could be

considered as the transverse mode of relatively long AgNWs.



6 Journal of Nanomaterials

0

0.5

1

1.5

2

300 350 400 450 500 550 600

Ab
so

rb
an

ce
(a

.u
.)

Wavelength (nm)
?0.5

?1

Ag 0.06M
Ag 0.08M

Figure 13: Absorption spectra of the as-synthesized AgNWs (with
NaClmediated agent) at differentmolar of AgNO

3
: (a) 0.06; (b) 0.08.

At this point, optical signatures similar to those of bulk Ag
also began to appear as indicated by the shoulder peak around
350 nm. As the concentration of AgNO

3
increases from 0.06

to 0.08M, the intensity of these peaks increased significantly
and red-shifted to 394 and 351 nm, respectively. This result
indicated that theAgNWs increased in numberwith apparent
growth in length. Again, the spectra show that the sample is
completely AgNWs.

4. Conclusion

AgNWs were successfully synthesized by using polyol tech-
niquewith andwithoutmediated agents. It was found that the
addition of CuCl

2
or NaCl to the polyol reduction of AgNO

3

in the presence of PVP greatly facilitated the formation of
AgNWs. Without the mediated agents, the final product
synthesized was a mixture of AgNps and AgNWs. Both the
cation and the anions are crucial for the successful production
of AgNWs.

Conflict of Interests

The authors declare that there is no conflict of interests
regarding the publication of this paper.

Acknowledgment

The financial support received from University of Malaya
Research Grant (RP011C-13AET) is gratefully acknowledged.

References

[1] Y. Xia, P. Yang, Y. Sun et al., ?One-dimensional nanostructures:
synthesis, characterization, and applications,? Advanced Mate-
rials, vol. 15, no. 5, pp. 353?389, 2003.

[2] Z. L. Wang, ?Characterizing the structure and properties of
individual wire-like nanoentities,? Advanced Materials, vol. 12,
no. 17, pp. 1295?1298, 2000.

[3] P. J. Cao, Y. S. Gu, H. W. Lin et al., ?High-density aligned
carbon nanotubes with uniformdiameters,? Journal ofMaterials
Research, vol. 18, pp. 1686?1690, 2003.

[4] J. Hu, T. W. Odom, and C. M. Lieber, ?Chemistry and physics
in one dimension: synthesis and properties of nanowires and
nanotubes,? Accounts of Chemical Research, vol. 32, no. 5, pp.
435?445, 1999.

[5] N. A. C. Lah and M. R. Johan, ?Facile shape control synthesis
and optical properties of silver nanoparticles stabilized by
Daxad 19 surfactant,?Applied Surface Science, vol. 257, no. 17, pp.
7494?7500, 2011.

[6] N. A. C. Lah and M. R. Johan, ?Optical and thermodynamic
studies of silver nanoparticles stabilized byDaxad 19 surfactant,?
International Journal of Materials Research, vol. 102, no. 3, pp.
340?347, 2011.

[7] C. M. Lieber, ?One-dimensional nanostructures: chemistry,
physics& applications,? Solid State Communications, vol. 107, no.
11, pp. 607?616, 1998.

[8] M. S. Gudiksen, L. J. Lauhon, J. Wang, D. C. Smith, and C. M.
Lieber, ?Growth of nanowire superlattice structures for nano-
scale photonics and electronics,? Nature, vol. 415, no. 6872, pp.
617?620, 2002.

[9] Y. Cui and C. M. Lieber, ?Functional nanoscale electronic
devices assembled using silicon nanowire building blocks,?
Science, vol. 291, no. 5505, pp. 851?853, 2001.

[10] C. R. Martin, ?Nanomaterials: a membrane-based synthetic
approach,? Science, vol. 266, no. 5193, pp. 1961?1966, 1994.

[11] A. M. Morales and C. M. Lieber, ?A laser ablation method for
the synthesis of crystalline semiconductor nanowires,? Science,
vol. 279, no. 5348, pp. 208?211, 1998.

[12] K. E. Korte, S. E. Skrabalak, and Y. J. Xia, ?Rapid synthesis of
silver nanowires through a cucl- or cucl2-mediated polyol
process,? Journal of Materials Chemistry, vol. 18, pp. 437?441,
2008.

[13] Y. Xia and Y. Sun, ?Large-scale synthesis of uniform silver
nanowires through a soft, self-seeding, polyol process,?
Advanced Materials, vol. 14, no. 11, pp. 833?837, 2002.

[14] Y. Sun, Y. Yin, B. T. Mayers, T. Herricks, and Y. Xia, ?Uniform
silver nanowires synthesis by reducing AgNO

3
with ethylene

glycol in the presence of seeds and poly(vinyl pyrrolidone),?
Chemistry of Materials, vol. 14, no. 11, pp. 4736?4745, 2002.

[15] K. K. Coswell, C. M. Bender, and C. J. Murply, ?Seedless,
surfactantless wet chemical synthesis of silver nanowires,?Nano
Letters, vol. 3, no. 5, pp. 667?669, 2003.

[16] P. S. Mdluli and N. Revaprasadu, ?An improved N,N-dim-
ethylformamide and polyvinyl pyrrolidone approach for the
synthesis of long silver nanowires,? Journal of Alloys and
Compounds, vol. 469, no. 1-2, pp. 519?522, 2009.

[17] Z. Wang, J. Liu, X. Chen, J. Wan, and Y. Qian, ?A simple hyd-
rothermal route to large-scale synthesis of uniform silver
nanowires,? Chemistry?A European Journal, vol. 11, no. 1, pp.
160?163, 2005.

[18] J. Xu, J. Hu, C. Peng, H. Liu, and Y. Hu, ?A simple approach to
the synthesis of silver nanowires by hydrothermal process in the
presence of gemini surfactant,? Journal of Colloid and Interface
Science, vol. 298, no. 2, pp. 689?693, 2006.

[19] Y. Zhou, S. H. Yu, C. Y. Wang, X. G. Li, Y. R. Zhu, and Z.
Y. Chen, ?A novel ultraviolet irradiation photoreduction tech-
nique for the preparation of single-crystal Ag nanorods and Ag
dendrites,?AdvancedMaterials, vol. 11, no. 10, pp. 850?852, 1999.

[20] K. Zou, X. H. Zhang, X. F. Duan, X. M. Meng, and S. K.
Wu, ?Seed-mediated synthesis of silver nanostructures and



Journal of Nanomaterials 7

polymer/ silver nanocables by UV irradiation,? Journal of Crys-
tal Growth, vol. 273, no. 1-2, pp. 285?291, 2004.

[21] A. R. Roosen andW. C. Carter, ?Simulations of microstructural
evolution: anisotropic growth and coarsening,? Physica A, vol.
261, no. 1-2, pp. 232?247, 1998.

[22] W. A. Al-Saidi, H. Feng, and K. A. Fichthorn, ?Adsorption of
polyvinylpyrrolidone on Ag surfaces: insight into a structure-
directing agent,? Nano Letters, vol. 12, no. 2, pp. 997?1001, 2012.

[23] W. A. Al-Saidi, H. Feng, and K. A. Fichthorn, ?The binding of
PVP to Ag surfaces: insight into a structure-directing agent
from dispersion-corrected density-functional theory,?The Jour-
nal of Physical Chemistry C, vol. 117, pp. 1163?1171, 2013.

[24] K. E. Korte, S. E. Skrabalak, andY. Xia, ?Rapid synthesis of silver
nanowires through a CuCl- or CuCl

2
-mediated polyol process,?

Journal of Materials Chemistry, vol. 18, no. 4, pp. 437?441, 2008.
[25] T. You, S. Xu, S. Sun, and X. Song, ?Controllable synthesis of

pentagonal silver nanowires via a simple alcohol-thermal
method,?Materials Letters, vol. 63, no. 11, pp. 920?922, 2009.



Submit your manuscripts at
http://www.hindawi.com

Scientifica
Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Corrosion
International Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Polymer Science
International Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Ceramics
Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Composites
Journal of

Nanoparticles
Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

International Journal of

Biomaterials

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Nanoscience
Journal of

Textiles
Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Journal of

Nanotechnology
Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Journal of

Crystallography
Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

The Scientific
World Journal
Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Coatings
Journal of

Advances in

Materials Science and Engineering
Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Smart Materials
Research

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Metallurgy
Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

BioMed
Research International

Materials
Journal of

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

N
a
no
m
a
te
ri
a
ls

Hindawi Publishing Corporation
http://www.hindawi.com Volume 2014

Journal ofNanomaterials




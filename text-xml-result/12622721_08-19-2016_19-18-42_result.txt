Action 1.Add
 diluted with 2-methoxyethanol(Aldrich) to 0.25 and 0.05wt%
Action 2.Add
 were further diluted to 0.05wt% with 2-methoxyethanol
Action 3.Add
 was further diluted to 0.05wt% with 2-methoxyethanol
Action 4.Synthesize
 A precursor ZnO solution was prepared
Action 5.Dissolve
 by dissolving zincacetatedihydrate(0.5g,99.999%tracemetalsbasis,Aldrich)in2-methoxyethanol(5.82mL,99.8%,anhydrous,Aldrich)
Action 6.Add
 by adding a small amount of ethanolamine(0.14mL,Aldrich) as a surfactant
Action 7.Synthesize
 Substrates were prepared specifically for AFM imaging
Action 8.Synthesize
 were prepared in the exact manner
Action 9.Dry
 dried in an 80 °C oven overnight in order
Action 10.Remove
 to evaporate the solvent
Action 11.Synthesize
 Substrates were prepared specifically for XPS and UPS analysis in the exact manner
Action 12.Wait
 followed by thermal annealing at 250 °C for 1 h
Action 13.Synthesize
 PEI and PEIE solutions were prepared
Action 14.Dissolve
 by dissolving each polymer in 2-methoxyethanol
Action 15.Concentrate
 concentration
Action 16.Wait
 thermally annealed at 110 °C for 10 min
Action 17.Synthesize
 A solution of PBDTT-FTTE was prepared in anargon-filledglovebox
Action 18.Dissolve
 by dissolving in a mixture of 1,2-dichlorobenzene and diiodooctane ( 100 : 3 v/v )
Action 19.Stir
 After stirring overnight with light
Action 20.Heat
 heating at 70 °C
Action 21.Add
 the PBDTT-FTTE solution was added to dry PC70BM at a 1 : 1.8 weight ratio
Action 22.Yield
 to give
Action 23.Concentrate
 an overall concentration of 40mgmL-1
Action 24.Stir
 to stir overnight
Action 25.Heat
 while heated at 70 °C
Action 26.Yield
 to give an 80-100 nm thick active layer
Action 27.Wait
 to sit for 1 h in the argon glovebox
Action 28.Remove
 before being transferred to a thermal evaporator where 7.5 nm of MoO3 and 100 nm of Ag
Action 29.Yield
 to form the anode
Action 30.Remove
 After evaporation of the anode
Action 31.Yield
 Current-voltage curves were obtained with a semiconductor parameter analyzer ( model HP4155A , Yokogawa Hewlett-Packard , Japan ) , and EQE data
Action 32.Recover
 were collected
Action 33.Recover
 Temporal stability data were collected

======================================================================================

NounPhrase 0
 4 .
NounPhrase 1
 EXPERIMENTAL SECTION 4.1
NounPhrase 2
 Materials
NounPhrase 3
 PEI[800] ( Mw ≈ 800g mol-1 , Aldrich ) and PEI[25k] ( Mw ≈ 25 000g mol-1 , Aldrich )
NounPhrase 4
 PEI[2k] ( Mw ≈ 2000g mol-1 , Aldrich ) and PEI[750k] ( Mw ≈ 750 000g mol-1 , Aldrich )
NounPhrase 5
 PEIE ( Mw ≈ 70 000g mol-1 , Aldrich )
NounPhrase 6
 PBDTT-FTTE and PC70BM
NounPhrase 7
 ( Solarmer Energy , Inc . , El Monte , CA )
NounPhrase 8
 4.2
NounPhrase 9
 AFM and TEM Images
NounPhrase 10
 AFM images
NounPhrase 11
 mode
NounPhrase 12
 the bulk morphology of the PEI series , 10wt% solutions of the polymers in 2-methoxyethanol
NounPhrase 13
 drop
NounPhrase 14
 This provided thin films approximately 40-60 nm
NounPhrase 15
 The instrument
NounPhrase 16
 an FEI Tecnai G2 F20 TEM
NounPhrase 17
 Standard exposure ( 0.1 s ) and acquisition times ( 2 s )
NounPhrase 18
 4.3
NounPhrase 19
 Photoelectron Spectroscopy Measurements
NounPhrase 20
 Measurements
NounPhrase 21
 XPS measurements
NounPhrase 22
 a monochromatized Al X-ray source
NounPhrase 23
 a hybrid lens
NounPhrase 24
 a slot aperture
NounPhrase 25
 UPS
NounPhrase 26
 a He lamp ( hν = 21.22 eV )
NounPhrase 27
 a hybrid lens
NounPhrase 28
 a 110 µm aperture
NounPhrase 29
 4.4
NounPhrase 30
 OPV and SCLC Device Fabrication
NounPhrase 31
 ITO-coated substrates ( 15 Ω □-1 , Shanghai B . Tree Tech , Shanghai , China )
NounPhrase 32
 water , and isopropylalcohol
NounPhrase 33
 The ZnO precursor solution
NounPhrase 34
 spin
NounPhrase 35
 Each PEI or PEIE solution
NounPhrase 36
 spin
NounPhrase 37
 a concentration of 15mgmL-1
NounPhrase 38
 The blend solution
NounPhrase 39
 spin
NounPhrase 40
 The films
NounPhrase 41
 vacuum
NounPhrase 42
 the devices
NounPhrase 43
 device architecture
NounPhrase 44
 ITO/ZnO/PBDTT-FTTE
NounPhrase 45
 PC70BM/MoO3/Ag or ITO/ZnO/PEI(E)/PBDTT-FTTE
NounPhrase 46
 PC70BM/MoO3/Ag
NounPhrase 47
 Each substrate
NounPhrase 48
 five pixels
NounPhrase 49
 the devices
NounPhrase 50
 A solar simulator ( model 16S , Solar Light Co . , Philadelphia , PA ) and 200 W xenon lamp power supply ( model XPS 200 , Solar Light Co . , Philadelphia , PA )
NounPhrase 51
 an NREL-certified Si photodiode ( model 1787-04 , Hamamatsu Photonics K . K . , Japan )
NounPhrase 52
 a solar cell spectral response measurement system ( model QEX10 , PV Measurements , Inc . , Boulder , CO )
NounPhrase 53
 This system’s calibration photodiode
NounPhrase 54
 The system
NounPhrase 55
 the same OSCs
NounPhrase 56
 electron-only SCLC devices
NounPhrase 57
 the hole-collecting MoO3/Ag anode
NounPhrase 58
 device architecture of ITO/ZnO/PEI(E)/PBDTT-FTTE
NounPhrase 59
 PC71BM/LiF/Al
NounPhrase 60
 SCLC devices

======================================================================================

VerbPhrase 0
 were received as pure polymers
VerbPhrase 1
 were received as 50wt%aqueoussolutions
VerbPhrase 2
 was received as a 35-40wt% aqueous solution
VerbPhrase 3
 were purchased from Solarmer
VerbPhrase 4
 used
VerbPhrase 5
 as received
VerbPhrase 6
 were acquired with a Dimension 3100 SPM ( Veeco Instruments Inc . , Woodbury , NY )
VerbPhrase 7
 operated
VerbPhrase 8
 in tapping
VerbPhrase 9
 described below for OSC fabrication
VerbPhrase 10
 were
VerbPhrase 11
 cast onto TEM grids ( Electron Microscopy Sciences , Hatfield , PA )
VerbPhrase 12
 used was
VerbPhrase 13
 operated at an accelerating voltage of 200 kV and in bright-field mode
VerbPhrase 14
 were used
VerbPhrase 15
 described below for OSC fabrication
VerbPhrase 16
 were conducted with a Kratos Axis Ultra DLD with an angle of photoelectron emission normal to the surface
VerbPhrase 17
 were conducted using
VerbPhrase 18
 operated at 10 mA and 15 kV with a spectrometer pass energy of 80 eV
VerbPhrase 19
 was conducted using
VerbPhrase 20
 operated at 20 W , with a spectrometer pass energy of 5 eV
VerbPhrase 21
 were cleaned by ultrasonication in acetone
VerbPhrase 22
 deionized
VerbPhrase 23
 followed by 60 s of plasma cleaning
VerbPhrase 24
 was
VerbPhrase 25
 coated on the cleaned ITO substrates at 5000 rpm for 40 s
VerbPhrase 26
 to achieve
VerbPhrase 27
 was
VerbPhrase 28
 coated on the ITO/ZnO substrate at 5000 rpm for 40 s
VerbPhrase 29
 to achieve
VerbPhrase 30
 again allowed
VerbPhrase 31
 was
VerbPhrase 32
 coated at 1000 rpm for 15
VerbPhrase 33
 s onto the ITO/ZnO or ITO/ZnO/PEI(E) substrates
VerbPhrase 34
 were allowed
VerbPhrase 35
 were
VerbPhrase 36
 deposited through a shadow mask
VerbPhrase 37
 was
VerbPhrase 38
 contained
VerbPhrase 39
 were measured under AM 1.5G solar illumination at a standard intensity of 100 mW/cm2
VerbPhrase 40
 were calibrated using
VerbPhrase 41
 using
VerbPhrase 42
 was calibrated against NISTstandard I755 with transfer uncertainty less than 0.5% between 400 and 1000 nm and less than 1% at all other wavelengths
VerbPhrase 43
 was operated in ac mode with a chopping frequency of 100 Hz
VerbPhrase 44
 by measuring
VerbPhrase 45
 were stored in ambient laboratory conditions with no additional light , moisture , or oxygen protection
VerbPhrase 46
 To make
VerbPhrase 47
 was replaced with 1.5 nm of LiF and 100 nm of Al
VerbPhrase 48
 were measured under dark conditions

======================================================================================

PrepPhrase 0
 with an OTESPA tip
PrepPhrase 1
 To image
PrepPhrase 2
 with an active area of 4 mm2
PrepPhrase 3
 at regular intervals
PrepPhrase 4
 Between measurements , OSCs
